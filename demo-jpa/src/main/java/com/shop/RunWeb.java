package com.shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RunWeb {
	public static void main(String[] args) {
		SpringApplication.run(RunWeb.class, args);
	}
}