package com.shop.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shop.entity.User;
import com.shop.repository.UserDao;

/**
 * @author liyao
 * @createTime 2018/8/13
 * @description
 */
@Service
@Transactional
public class UserService {

    @Autowired
    private UserDao userDao;

    /**
     * 保存数据
     * @param user
     */
    public User save(User user){
        return userDao.save(user);
    }

    /**
     * 删除数据
     * @param id
     * @return
     */
    public  List<User> delete(Long id){
        userDao.deleteById(id);
        return userDao.findAll();
    }

    /**
     * 查询数据
     * @return
     */
    public List<User> list(){
        return userDao.findAll();
    }
}