package com.shop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shop.entity.User;
import com.shop.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;

	/**
	 * 查询用户列表
	 * 
	 * @return
	 */
	@GetMapping(value = "/list")
	public List<User> list() {
		return userService.list();
	}

	/**
	 * 添加、更新用户的方法
	 * 
	 * @param entity
	 * @return
	 */
	@GetMapping(value = "/save")
	public User save(User entity) {
		return userService.save(entity);
	}

	/**
	 * 删除用户
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping(value = "/delete")
	public List<User> delete(Long id) {
		userService.delete(id);
		return userService.list();
	}

}