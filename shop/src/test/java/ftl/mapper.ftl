package com.shop.mapper.${packagePath};

import com.shop.common.BaseMapper;
import com.shop.entity.${packagePath}.${className};

public interface ${className}Mapper extends BaseMapper<${className}> {


}