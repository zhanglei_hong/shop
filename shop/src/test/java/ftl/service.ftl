package com.shop.service.${packagePath};


import com.shop.entity.${packagePath}.${className};
import com.shop.service.base.BaseService;

public interface ${className}Service extends BaseService<${className}> {

}