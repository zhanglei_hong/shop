package com.shop.service.impl.${packagePath};

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.shop.entity.${packagePath}.${className};
import com.shop.mapper.${packagePath}.${className}Mapper;
import com.shop.service.base.BaseServiceImpl;
import com.shop.service.${packagePath}.${className}Service;

@Service
public class ${className}ServiceImpl extends BaseServiceImpl<${className}> implements ${className}Service {
	@Autowired
	private ${className}Mapper ${objectName}Mapper;

 
}