package template;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class TemplateRun {
	static String url = "jdbc:mysql://localhost:3306/shop?"
			+ "user=root&password=123456&serverTimezone=UTC&useUnicode=true&characterEncoding=UTF8";

	private static String path = "src/main/java/com/shop";

	// 数据库表
	private static String tableName = "shop_details";
	private static String packagePath = "shop";
	// 类名字

	public static void main(String[] args) throws Exception {
		setModel();
	}

	public static void setModel() throws Exception {
		// 写入模板数据
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("columnList", getColumnList());
		String className = StringUtils.capitalize(replaceUnderLineAndUpperCase(tableName));
		// 类名
		map.put("className", className);
		// 变量名字首字母小写
		map.put("objectName", replaceUnderLineAndUpperCase(tableName));
		map.put("tableName", tableName);
		map.put("packagePath", packagePath);

		Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
		configuration.setDirectoryForTemplateLoading(new File("src/test/java/ftl"));

		{
			Template template = configuration.getTemplate("entity.ftl");
			String fileName = className + ".java";
			File file = new File(path + "/entity/" + packagePath);
			if (!file.exists()) {
				file.mkdirs();
			}
			Writer writer = new FileWriter(path + "/entity/" + packagePath + "/" + fileName);
			template.process(map, writer);
			writer.flush();
			writer.close();
		}
		{
			Template template = configuration.getTemplate("mapper.ftl");
			String fileName = className + "Mapper.java";
			File file = new File(path + "/mapper/" + packagePath);
			if (!file.exists()) {
				file.mkdirs();
			}
			Writer writer = new FileWriter(path + "/mapper/" + packagePath + "/" + fileName);
			template.process(map, writer);
			writer.flush();
			writer.close();
		}
		{
			Template template = configuration.getTemplate("service.ftl");
			String fileName = className + "Service.java";
			File file = new File(path + "/service/" + packagePath);
			if (!file.exists()) {
				file.mkdirs();
			}
			Writer writer = new FileWriter(path + "/service/" + packagePath + "/" + fileName);
			template.process(map, writer);
			writer.flush();
			writer.close();
		}
		{
			Template template = configuration.getTemplate("serviceImpl.ftl");
			String fileName = className + "ServiceImpl.java";
			File file = new File(path + "/service/impl/" + packagePath);
			if (!file.exists()) {
				file.mkdirs();
			}
			Writer writer = new FileWriter(path + "/service/impl/" + packagePath + "/" + fileName);
			template.process(map, writer);
			writer.flush();
			writer.close();
		}
		{
			Template template = configuration.getTemplate("controller.ftl");
			String fileName = className + "Controller.java";
			File file = new File(path + "/controller/" + packagePath);
			if (!file.exists()) {
				file.mkdirs();
			}
			Writer writer = new FileWriter(path + "/controller/" + packagePath + "/" + fileName);
			template.process(map, writer);
			writer.flush();
			writer.close();
		}
		{
			Template template = configuration.getTemplate("xml.ftl");
			String fileName = className + "Mapper.xml";
			File file = new File("src/main/resources/mapper/" + packagePath);
			if (!file.exists()) {
				file.mkdirs();
			}
			Writer writer = new FileWriter("src/main/resources/mapper/" + packagePath + "/" + fileName);
			template.process(map, writer);
			writer.flush();
			writer.close();
		}

	}

	public static List<Map<String, String>> getColumnList() {
		List<Map<String, String>> columnList = new ArrayList<Map<String, String>>();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conn = DriverManager.getConnection(url);
			PreparedStatement preparedStatement = null;
			String sql = "SELECT column_name, data_type, column_comment, CONCAT(CONCAT('\\#{',column_name), '\\}') as column_value FROM Information_schema.columns 	WHERE table_Name =? ";
			preparedStatement = conn.prepareStatement(sql);
			preparedStatement.setString(1, tableName);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				String columnName = rs.getString(1);
				String dataType = rs.getString(2);
				String columnComment = rs.getString(3);
				String columnValue = rs.getString(4);
				Map<String, String> map = new HashMap<String, String>();
				map.put("columnName", columnName);
				map.put("changeColumnName", replaceUnderLineAndUpperCase(columnName));
				map.put("CapitalColumnName", StringUtils.capitalize(replaceUnderLineAndUpperCase(columnName)));
				map.put("dataType", dataType);

				String type = "String";
				if ("bigint".equals(dataType)) {
					type = "Long";
				} else if ("int".equals(dataType)) {
					type = "Integer";
				} else if (dataType.indexOf("date") > -1) {
					type = "Date";
				}
				map.put("type", type);
				map.put("columnComment", columnComment);
				map.put("columnValue", columnValue);
				columnList.add(map);
			}
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return columnList;
	}

	public static String replaceUnderLineAndUpperCase(String str) {
		StringBuffer sb = new StringBuffer();
		sb.append(str);
		int count = sb.indexOf("_");
		while (count != 0) {
			int num = sb.indexOf("_", count);
			count = num + 1;
			if (num != -1) {
				char ss = sb.charAt(count);
				char ia = (char) (ss - 32);
				sb.replace(count, count + 1, ia + "");
			}
		}
		String result = sb.toString().replaceAll("_", "");
		return result;
	}
}
