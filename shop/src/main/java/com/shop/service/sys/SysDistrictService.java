package com.shop.service.sys;


import java.util.List;
import java.util.Map;

import com.shop.entity.sys.SysDistrict;
import com.shop.service.base.BaseService;

public interface SysDistrictService extends BaseService<SysDistrict> {

	List<SysDistrict> getProvince();

	List<SysDistrict> getCity(Long pid);

	List<SysDistrict> getAllCity();

	Map<Long, SysDistrict> selectSysDistrictByIdMap(List<Long> sysDistrictIdList);

	List<SysDistrict> selectSysDistrictByIdList(List<Long> sysDistrictIdList);

}