package com.shop.service.base;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shop.common.BaseMapper;

@Service
public abstract class BaseServiceImpl<T> implements BaseService<T> {

	@Autowired
	private BaseMapper<T> baseMapper;

	@Override
	public T selectOne(T record) {
		return baseMapper.selectOne(record);
	}

	@Override
	public List<T> select(T record) {
		return baseMapper.select(record);
	}

	@Override
	public List<T> selectAll() {
		return baseMapper.selectAll();
	}

	@Override
	public int selectCount(T record) {
		return baseMapper.selectCount(record);
	}

	@Override
	public T selectByPrimaryKey(Object key) {
		return baseMapper.selectByPrimaryKey(key);
	}

	@Override
	public boolean existsWithPrimaryKey(Object key) {
		return baseMapper.existsWithPrimaryKey(key);
	}

	@Override
	public int insert(T record) {
		return baseMapper.insert(record);
	}

	@Override
	public int insertSelective(T record) {
		return baseMapper.insertSelective(record);
	}

	@Override
	public int updateByPrimaryKey(T record) {
		return baseMapper.updateByPrimaryKey(record);
	}

	@Override
	public int updateByPrimaryKeySelective(T record) {
		return baseMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int delete(T record) {
		return baseMapper.delete(record);
	}

	@Override
	public int deleteByPrimaryKey(Object key) {
		return baseMapper.deleteByPrimaryKey(key);
	}

	@Override
	public List<T> selectByExample(Object example) {
		return baseMapper.selectByExample(example);
	}

	@Override
	public T selectOneByExample(Object example) {
		return baseMapper.selectOneByExample(example);
	}

	@Override
	public int selectCountByExample(Object example) {
		return baseMapper.selectCountByExample(example);
	}

	@Override
	public int deleteByExample(Object example) {
		return baseMapper.deleteByExample(example);
	}

	@Override
	public int updateByExample(T record, Object example) {
		return baseMapper.updateByExample(record, example);
	}

	@Override
	public int updateByExampleSelective(T record, Object example) {
		return baseMapper.updateByExampleSelective(record, example);
	}

	@Override
	public List<T> selectByExampleAndRowBounds(Object example, RowBounds rowBounds) {
		return baseMapper.selectByExampleAndRowBounds(example, rowBounds);
	}

	@Override
	public List<T> selectByRowBounds(T record, RowBounds rowBounds) {
		return baseMapper.selectByExampleAndRowBounds(record, rowBounds);
	}

	@Override
	public int insertList(List<? extends T> recordList) {
		return baseMapper.insertList(recordList);
	}

	@Override
	public int insertUseGeneratedKeys(T record) {
		return baseMapper.insertUseGeneratedKeys(record);
	}

}
