package com.shop.service.impl.sys;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shop.entity.sys.SysDistrict;
import com.shop.mapper.sys.SysDistrictMapper;
import com.shop.service.base.BaseServiceImpl;
import com.shop.service.sys.SysDistrictService;

import tk.mybatis.mapper.entity.Example;

@Service
public class SysDistrictServiceImpl extends BaseServiceImpl<SysDistrict> implements SysDistrictService {
	@Autowired
	private SysDistrictMapper sysDistrictMapper;

	@Override
	public List<SysDistrict> getProvince() {
		return getCity(1l);
	}

	@Override
	public List<SysDistrict> getCity(Long pid) {
		Example example = new Example(SysDistrict.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andEqualTo("pid", pid);
		List<SysDistrict> list = sysDistrictMapper.selectByExample(example);
		return list;
	}

	@Override
	public List<SysDistrict> getAllCity() {
		Example example = new Example(SysDistrict.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andEqualTo("level", 2);
		List<SysDistrict> list = sysDistrictMapper.selectByExample(example);
		return list;
	}

	@Override
	public Map<Long, SysDistrict> selectSysDistrictByIdMap(List<Long> sysDistrictIdList) {
		List<SysDistrict> list = selectSysDistrictByIdList(sysDistrictIdList);
		Map<Long, SysDistrict> map = list.stream().collect(Collectors.toMap(SysDistrict::getId, Function.identity()));
		return map;
	}
	@Override
	public List<SysDistrict> selectSysDistrictByIdList(List<Long> sysDistrictIdList) {
		if (sysDistrictIdList == null || sysDistrictIdList.size() < 1) {
			return new ArrayList<SysDistrict>();
		}
		Example example = new Example(SysDistrict.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andIn("id", sysDistrictIdList);
		List<SysDistrict> list = sysDistrictMapper.selectByExample(example);
		return list;
	}
	
	
}