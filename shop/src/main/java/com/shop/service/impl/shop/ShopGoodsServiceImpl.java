package com.shop.service.impl.shop;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shop.common.Env;
import com.shop.common.Page;
import com.shop.entity.shop.ShopGoods;
import com.shop.mapper.shop.ShopGoodsMapper;
import com.shop.service.base.BaseServiceImpl;
import com.shop.service.shop.ShopGoodsService;

import tk.mybatis.mapper.entity.Example;

@Service
public class ShopGoodsServiceImpl extends BaseServiceImpl<ShopGoods> implements ShopGoodsService {
	@Autowired
	private ShopGoodsMapper shopGoodsMapper;

	@Override
	public PageInfo<ShopGoods> getShopGoodsList(Page page, ShopGoods data) {
		Example example = new Example(ShopGoods.class);
		Example.Criteria criteria = example.createCriteria();
		String name = data.getName();
		if (name != null && !"".equals(name)) {
			criteria.andLike("name", "%" + name + "%");
		}
		criteria.andEqualTo("status", 1);
		PageHelper.startPage(page.getPage(), page.getRows(), true);
		List<ShopGoods> list = shopGoodsMapper.selectByExample(example);
		list.forEach(it->{
			it.setImg(Env.getFileUrl()+"/"+it.getImg());
		});
		PageInfo<ShopGoods> pageInfo = new PageInfo<ShopGoods>(list);
		return pageInfo;
	}

}