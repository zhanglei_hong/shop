package com.shop.service.impl.shop;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shop.common.Page;
import com.shop.entity.shop.ShopRecruitment;
import com.shop.mapper.shop.ShopRecruitmentMapper;
import com.shop.service.base.BaseServiceImpl;
import com.shop.service.shop.ShopRecruitmentService;

import tk.mybatis.mapper.entity.Example;

@Service
public class ShopRecruitmentServiceImpl extends BaseServiceImpl<ShopRecruitment> implements ShopRecruitmentService {
	@Autowired
	private ShopRecruitmentMapper shopRecruitmentMapper;

	@Override
	public PageInfo<ShopRecruitment> getShopRecruitmentList(Page page, ShopRecruitment data) {
		PageHelper.startPage(page.getPage(), page.getRows(), true);

		Example example = new Example(ShopRecruitment.class);
		Example.Criteria criteria = example.createCriteria();
		String name = data.getName();
		if (name != null && !"".equals(name)) {
			criteria.andLike("name", "%" + name + "%");
		}

		Long province = data.getProvince();

		if (province != null) {
			criteria.andEqualTo("province", province);
		}
		
		Long city = data.getCity();
		if (city != null) {
			criteria.andEqualTo("city", city);
		}

		Integer experience = data.getExperience();

		if (experience != null) {
			criteria.andEqualTo("experience", experience);
		}

		Integer education = data.getEducation();
		if (education != null) {
			criteria.andEqualTo("education", education);
		}

		criteria.andEqualTo("status", 1);
		
		example.orderBy("roof").desc();
		example.orderBy("updateTime").desc();
		
		List<ShopRecruitment> list = shopRecruitmentMapper.selectByExample(example);
		PageInfo<ShopRecruitment> pageInfo = new PageInfo<ShopRecruitment>(list);
		return pageInfo;
	}

}