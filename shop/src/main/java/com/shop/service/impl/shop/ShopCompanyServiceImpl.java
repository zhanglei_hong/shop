package com.shop.service.impl.shop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.shop.entity.shop.ShopCompany;
import com.shop.entity.shop.ShopGoods;
import com.shop.mapper.shop.ShopCompanyMapper;
import com.shop.service.base.BaseServiceImpl;
import com.shop.service.shop.ShopCompanyService;

import tk.mybatis.mapper.entity.Example;

@Service
public class ShopCompanyServiceImpl extends BaseServiceImpl<ShopCompany> implements ShopCompanyService {
	@Autowired
	private ShopCompanyMapper shopCompanyMapper;

	@Override
	public Map<Long, ShopCompany> selectByCompanyIdMap(List<Long> companyIdList) {
		if (companyIdList == null || companyIdList.size() < 1) {
			return new HashMap<Long, ShopCompany>();
		}
		List<ShopCompany> list = selectByCompanyIdList(companyIdList);
		Map<Long, ShopCompany> map = list.stream().collect(Collectors.toMap(ShopCompany::getId, Function.identity()));
		return map;
	}

	@Override
	public List<ShopCompany> selectByCompanyIdList(List<Long> companyIdList) {
		if (companyIdList == null || companyIdList.size() < 1) {
			return new ArrayList<ShopCompany>();
		}
		Example example = new Example(ShopGoods.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andIn("id", companyIdList);
		List<ShopCompany> list = shopCompanyMapper.selectByExample(example);
		return list;
	}
}