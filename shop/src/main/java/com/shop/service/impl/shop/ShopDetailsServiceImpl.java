package com.shop.service.impl.shop;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shop.common.Env;
import com.shop.common.Page;
import com.shop.entity.shop.ShopDetails;
import com.shop.mapper.shop.ShopDetailsMapper;
import com.shop.service.base.BaseServiceImpl;
import com.shop.service.shop.ShopDetailsService;

import tk.mybatis.mapper.entity.Example;

@Service
public class ShopDetailsServiceImpl extends BaseServiceImpl<ShopDetails> implements ShopDetailsService {
	@Autowired
	private ShopDetailsMapper shopDetailsMapper;

	@Override
	public PageInfo<ShopDetails> getShopDetailsList(Page page, ShopDetails data) {

		PageHelper.startPage(page.getPage(), page.getRows(), true);
		Example example = new Example(ShopDetails.class);
		Example.Criteria criteria = example.createCriteria();
		String name = data.getName();
		if (name != null && !"".equals(name)) {
			criteria.andLike("name", "%" + name + "%");
		}
		criteria.andEqualTo("status", 1);
		List<ShopDetails> list = shopDetailsMapper.selectByExample(example);
		list.forEach(it->{
			it.setImg(Env.getFileUrl()+"/"+it.getImg());
		});
		PageInfo<ShopDetails> pageInfo = new PageInfo<ShopDetails>(list);
		return pageInfo;
	
	}

	
	 
}