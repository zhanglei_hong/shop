package com.shop.service.impl.wx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shop.entity.wx.WxUser;
import com.shop.mapper.wx.WxUserMapper;
import com.shop.service.base.BaseServiceImpl;
import com.shop.service.wx.WxUserService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class WxUserServiceImpl extends BaseServiceImpl<WxUser> implements WxUserService {
	@Autowired
	private WxUserMapper wxUserMapper;

	@Override
	public WxUser insertOrSelective(WxUser wxUser) {
		String openid = wxUser.getOpenid();
		Example example = new Example(WxUser.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("openid", openid);
		WxUser wxUserDb = wxUserMapper.selectOneByExample(example);
		if (wxUserDb != null) {
			wxUserMapper.updateByExampleSelective(wxUser, example);
			return wxUserDb;
		}
		wxUserMapper.insertSelective(wxUser);
		return wxUser;
	}

}