package com.shop.service.impl.shop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.shop.entity.shop.ShopGoodsClass;
import com.shop.mapper.shop.ShopGoodsClassMapper;
import com.shop.service.base.BaseServiceImpl;
import com.shop.service.shop.ShopGoodsClassService;

@Service
public class ShopGoodsClassServiceImpl extends BaseServiceImpl<ShopGoodsClass> implements ShopGoodsClassService {
	@Autowired
	private ShopGoodsClassMapper shopGoodsClassMapper;

 
}