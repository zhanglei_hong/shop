package com.shop.service.wx;


import com.shop.entity.wx.WxUser;
import com.shop.service.base.BaseService;

public interface WxUserService extends BaseService<WxUser> {

	WxUser insertOrSelective(WxUser wxUser);

}