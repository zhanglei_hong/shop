package com.shop.service.shop;


import com.shop.entity.shop.ShopGoodsClass;
import com.shop.service.base.BaseService;

public interface ShopGoodsClassService extends BaseService<ShopGoodsClass> {

}