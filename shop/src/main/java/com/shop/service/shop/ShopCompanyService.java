package com.shop.service.shop;


import java.util.List;
import java.util.Map;

import com.shop.entity.shop.ShopCompany;
import com.shop.service.base.BaseService;

public interface ShopCompanyService extends BaseService<ShopCompany> {

	Map<Long, ShopCompany> selectByCompanyIdMap(List<Long> companyIdList);

	List<ShopCompany> selectByCompanyIdList(List<Long> companyIdList);

}