package com.shop.service.shop;


import com.github.pagehelper.PageInfo;
import com.shop.common.Page;
import com.shop.entity.shop.ShopGoods;
import com.shop.service.base.BaseService;

public interface ShopGoodsService extends BaseService<ShopGoods> {

	PageInfo<ShopGoods> getShopGoodsList(Page page, ShopGoods data);

}