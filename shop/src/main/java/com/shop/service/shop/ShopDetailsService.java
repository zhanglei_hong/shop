package com.shop.service.shop;


import com.github.pagehelper.PageInfo;
import com.shop.common.Page;
import com.shop.entity.shop.ShopDetails;
import com.shop.service.base.BaseService;

public interface ShopDetailsService extends BaseService<ShopDetails> {

	PageInfo<ShopDetails> getShopDetailsList(Page page, ShopDetails data);

}