package com.shop.service.shop;


import com.github.pagehelper.PageInfo;
import com.shop.common.Page;
import com.shop.entity.shop.ShopRecruitment;
import com.shop.service.base.BaseService;

public interface ShopRecruitmentService extends BaseService<ShopRecruitment> {

	PageInfo<ShopRecruitment> getShopRecruitmentList(Page page, ShopRecruitment data);

}