package com.shop.mapper.shop;

import com.shop.common.BaseMapper;
import com.shop.entity.shop.ShopCompany;

public interface ShopCompanyMapper extends BaseMapper<ShopCompany> {


}