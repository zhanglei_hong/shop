//package com.shop.filter;
//
//import java.util.Date;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.HandlerInterceptor;
//import org.springframework.web.servlet.ModelAndView;
//
//import com.shop.entity.wx.WxUser;
//import com.shop.feign.dto.UserInfo;
//import com.shop.feign.impl.WechatFeignImpl;
//import com.shop.service.wx.WxUserService;
//
//@Component
//public class WeChatHandlerInterceptor implements HandlerInterceptor {
//	@Autowired
//	private WxUserService wxUserService;
//
//	@Override
//	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
//			throws Exception {
//		WxUser wxUser = (WxUser) request.getSession().getAttribute("wxUser");
//		if (wxUser != null) {
//			request.getSession().setAttribute("wxUser", wxUser);
//			return true;
//		}
//		String code = request.getParameter("code");
//		if (code != null && !"".equals(code)) {
//			// 插入数据库
//			// 写入session
//			UserInfo userInfo = WechatFeignImpl.getUserInfo(code);
//			wxUser = new WxUser();
//			wxUser.setCity(userInfo.getCity());
//			wxUser.setCountry(userInfo.getCountry());
//			wxUser.setCreateTime(new Date());
//			wxUser.setHeadimgurl(userInfo.getHeadimgurl());
//			wxUser.setNickname(userInfo.getNickname());
//			wxUser.setOpenid(userInfo.getOpenid());
//			wxUser.setPrivilege(String.join(",", userInfo.getPrivilege()));
//			wxUser.setProvince(userInfo.getProvince());
//			wxUser.setSex(userInfo.getSex());
//			wxUser.setUnionid(userInfo.getUnionid());
//			wxUser.setUpdateTime(new Date());
//			// 修改或者插入新数据
//			wxUser = wxUserService.insertOrSelective(wxUser);
//			request.getSession().setAttribute("wxUser", wxUser);
//			return true;
//		}
//		return false;
//	}
//
//	@Override
//	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
//			ModelAndView modelAndView) throws Exception {
//
//	}
//
//	@Override
//	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
//			throws Exception {
//
//	}
//}