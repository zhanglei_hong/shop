//package com.shop.filter;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
//
//@Configuration
//public class WebConfig extends WebMvcConfigurationSupport {
//
//	@Bean
//	public WeChatHandlerInterceptor getMyInterceptor() {
//		return new WeChatHandlerInterceptor();
//	}
//
//	@Override
//	protected void addInterceptors(InterceptorRegistry registry) {
//		registry.addInterceptor(getMyInterceptor()).addPathPatterns("/**").excludePathPatterns("/css/*", "/js/*");
//		super.addInterceptors(registry);
//	}
//}