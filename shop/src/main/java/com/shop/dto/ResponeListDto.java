package com.shop.dto;

import com.github.pagehelper.PageInfo;

public class ResponeListDto<T, E> {

	private PageInfo<T> list;
	private E data;

	public PageInfo<T> getList() {
		return list;
	}

	public void setList(PageInfo<T> list) {
		this.list = list;
	}

	public E getData() {
		return data;
	}

	public void setData(E data) {
		this.data = data;
	}

}