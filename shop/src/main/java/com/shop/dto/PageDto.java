package com.shop.dto;

import com.shop.common.Page;

public class PageDto<T> {

	private Page page;
	
	private T data;

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	
}
