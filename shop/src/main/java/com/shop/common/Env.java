package com.shop.common;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:env.properties")
public class Env {

	private static String appId;
	private static String appSecret;

	@Value("${appIdOrg}")
	private String appIdOrg;

	@Value("${appSecretOrg}")
	private String appSecretOrg;

	private static String filePath;

	@Value("${filePath}")
	private String filePathOrg;
	
	
	
	private static String fileUrl;

	@Value("${fileUrl}")
	private String fileUrlOrg;
	
	
	@PostConstruct
	public void initEnv() {
		appId = this.appIdOrg;
		appSecret = this.appSecretOrg;
		filePath = this.filePathOrg;
		fileUrl = this.fileUrlOrg;
	}

	public static String getAppId() {
		return appId;
	}

	public static String getAppSecret() {
		return appSecret;
	}
	public static String getFilePath() {
		return filePath;
	}

	public static String getFileUrl() {
		return fileUrl;
	}
	
	

}
