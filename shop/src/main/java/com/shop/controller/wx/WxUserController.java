package com.shop.controller.wx;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shop.entity.wx.WxUser;
import com.shop.feign.dto.UserInfo;
import com.shop.feign.impl.WechatFeignImpl;
import com.shop.service.wx.WxUserService;
import com.shop.util.LoginUtil;

@Controller
@RequestMapping("/wxUser")
public class WxUserController {

	@Autowired
	private WxUserService wxUserService;

	@GetMapping(value = "/getUser")
	@ResponseBody
	public WxUser getUser() {
		return LoginUtil.getSysUser();
	}

	@GetMapping(value = "/getCode")
	public String getCode(String code, String state, HttpSession session) {
		if (code == null || "".equals(code)) {
			return null;
		}
		// 插入数据库
		// 写入session
		UserInfo userInfo = WechatFeignImpl.getUserInfo(code);
		WxUser wxUser = new WxUser();
		wxUser.setCity(userInfo.getCity());
		wxUser.setCountry(userInfo.getCountry());
		wxUser.setCreateTime(new Date());
		wxUser.setHeadimgurl(userInfo.getHeadimgurl());
		wxUser.setNickname(userInfo.getNickname());
		wxUser.setOpenid(userInfo.getOpenid());
		wxUser.setPrivilege(String.join(",", userInfo.getPrivilege()));
		wxUser.setProvince(userInfo.getProvince());
		wxUser.setSex(userInfo.getSex());
		wxUser.setUnionid(userInfo.getUnionid());
		wxUser.setUpdateTime(new Date());
		// 修改或者插入新数据
		wxUser = wxUserService.insertOrSelective(wxUser);
		session.setAttribute("wxUser", wxUser);
		return "redirect:/index.html#/" + state;

	}

}
