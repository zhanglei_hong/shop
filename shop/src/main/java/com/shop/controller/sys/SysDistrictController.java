package com.shop.controller.sys;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shop.entity.sys.SysDistrict;
import com.shop.service.sys.SysDistrictService;

@RestController
@RequestMapping("/sysDistrict")
public class SysDistrictController {

	@Autowired
	private SysDistrictService sysDistrictService;

	@RequestMapping(value = "/getProvince")
	public List<SysDistrict> getProvince() {
		return sysDistrictService.getProvince();
	}

	@RequestMapping(value = "/getAllCity")
	public Map<Long, List<SysDistrict>> getAllCity() {
		 List<SysDistrict> list = sysDistrictService.getAllCity();
		 return list.stream().collect(Collectors.groupingBy(SysDistrict::getPid));
	}
}
