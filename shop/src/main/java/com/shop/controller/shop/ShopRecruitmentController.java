package com.shop.controller.shop;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.shop.dto.PageDto;
import com.shop.dto.ResponeDataDto;
import com.shop.dto.ResponeListDto;
import com.shop.entity.shop.ShopCompany;
import com.shop.entity.shop.ShopRecruitment;
import com.shop.entity.sys.SysDistrict;
import com.shop.service.shop.ShopCompanyService;
import com.shop.service.shop.ShopRecruitmentService;
import com.shop.service.sys.SysDistrictService;

@RestController
@RequestMapping("/shopRecruitment")
public class ShopRecruitmentController {

	@Autowired
	private ShopRecruitmentService shopRecruitmentService;

	@Autowired
	private ShopCompanyService shopCompanyService;
	
	
	@Autowired
	private SysDistrictService sysDistrictService;
	

	@PostMapping(value = "/getShopRecruitmentList")
	public ResponeListDto<ShopRecruitment, Map<Long, ShopCompany>> getShopRecruitmentList(
			@RequestBody PageDto<ShopRecruitment> pageDto) {
		PageInfo<ShopRecruitment> page = shopRecruitmentService.getShopRecruitmentList(pageDto.getPage(),
				pageDto.getData());
		List<ShopRecruitment> list = page.getList();
		List<Long> companyIdList = list.stream().map(ShopRecruitment::getCompanyId).collect(Collectors.toList());
		Map<Long, ShopCompany> shopCompanyMap = shopCompanyService.selectByCompanyIdMap(companyIdList);
		ResponeListDto<ShopRecruitment, Map<Long, ShopCompany>> responeDto = new ResponeListDto<ShopRecruitment, Map<Long, ShopCompany>>();
		responeDto.setList(page);
		responeDto.setData(shopCompanyMap);
		return responeDto;
	}
	
	@GetMapping(value = "/getShopRecruitmentById")
	public ResponeDataDto<ShopRecruitment,Map<Long, SysDistrict>> getShopRecruitmentById(Long id) {
		ShopRecruitment data = shopRecruitmentService.selectByPrimaryKey(id);
		List<Long> sysDistrictIdList =new ArrayList<Long>();
		sysDistrictIdList.add(data.getProvince());
		sysDistrictIdList.add(data.getCity());
		Map<Long, SysDistrict> sysDistrictMap = sysDistrictService.selectSysDistrictByIdMap(sysDistrictIdList);
		ResponeDataDto<ShopRecruitment,Map<Long, SysDistrict>>  resData=new ResponeDataDto<ShopRecruitment, Map<Long,SysDistrict>>();
		resData.setData(data);
		resData.setMap(sysDistrictMap);
		return resData;
	}
}
