package com.shop.controller.shop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.shop.common.Env;
import com.shop.dto.PageDto;
import com.shop.entity.shop.ShopDetails;
import com.shop.service.shop.ShopDetailsService;

@RestController
@RequestMapping("/shopDetails")
public class ShopDetailsController {

	@Autowired
	private ShopDetailsService shopDetailsService;

	@PostMapping(value = "/getShopDetailsList")
	public PageInfo<ShopDetails> getShopDetailsList(@RequestBody PageDto<ShopDetails> pageDto) {
		return shopDetailsService.getShopDetailsList(pageDto.getPage(), pageDto.getData());
	}	
	
	@GetMapping(value = "/getShopDetailsById")
	public ShopDetails getShopDetailsById(Long id) {
		ShopDetails data = shopDetailsService.selectByPrimaryKey(id);
		data.setImg(Env.getFileUrl()+"/"+data.getImg());
		return data;
	}
}
