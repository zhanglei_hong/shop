package com.shop.controller.shop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.shop.common.Env;
import com.shop.dto.PageDto;
import com.shop.entity.shop.ShopGoods;
import com.shop.service.shop.ShopGoodsService;

@RestController
@RequestMapping("/shopGoods")
public class ShopGoodsController {

	@Autowired
	private ShopGoodsService shopGoodsService;

	@PostMapping(value = "/getShopGoodsList")
	public PageInfo<ShopGoods> getShopGoodsList(@RequestBody PageDto<ShopGoods> pageDto) {
		return shopGoodsService.getShopGoodsList(pageDto.getPage(), pageDto.getData());
	}
	@GetMapping(value = "/getShopGoodsById")
	public ShopGoods getShopGoodsById(Long id) {
		ShopGoods data = shopGoodsService.selectByPrimaryKey(id);
		data.setImg(Env.getFileUrl()+"/"+data.getImg());
		return data;
	}
}
