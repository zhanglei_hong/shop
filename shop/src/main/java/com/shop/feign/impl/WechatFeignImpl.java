package com.shop.feign.impl;

import com.shop.common.Env;
import com.shop.feign.WechatFeign;
import com.shop.feign.dto.AccessToken;
import com.shop.feign.dto.UserInfo;

import feign.Feign;
import feign.Request;
import feign.Retryer;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;

public class WechatFeignImpl {

	static String host = "https://api.weixin.qq.com";

	static WechatFeign wechatFeign = Feign.builder().encoder(new JacksonEncoder()).decoder(new JacksonDecoder())
			.options(new Request.Options(1000, 3500)).retryer(new Retryer.Default(5000, 5000, 3))
			.target(WechatFeign.class, host);

	public static UserInfo getUserInfo(String code) {
		AccessToken accessToken = wechatFeign.getAccessTokenByCode(Env.getAppId(), Env.getAppSecret(), code);
		String access_token = accessToken.getAccess_token();
		String openid = accessToken.getOpenid();
		if (access_token != null && openid != null) {
			return wechatFeign.getUserInfo(access_token, openid);
		}
		return null;
	}
}
