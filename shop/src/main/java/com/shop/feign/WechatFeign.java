package com.shop.feign;

import com.shop.feign.dto.AccessToken;
import com.shop.feign.dto.UserInfo;

import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface WechatFeign {

	/**
	 * https://api.weixin.qq.com
	 * 
	 * @param appid  公众号的唯一标识
	 * @param secret 公众号的appsecret
	 * @param code   填写第一步获取的code参数
	 * @return
	 */
	@Headers({ "Content-Type: application/json", "Accept: application/json" })
	@RequestLine("GET /sns/oauth2/access_token?appid={appid}&secret={secret}&code={code}&grant_type=authorization_code")
	AccessToken getAccessTokenByCode(@Param(value = "appid") String appid, @Param(value = "secret") String secret,
			@Param(value = "code") String code);

	/**
	 * https://api.weixin.qq.com
	 * 
	 * @param access_token
	 * @param openid
	 * @return
	 */
	@Headers({ "Content-Type: application/json", "Accept: application/json" })
	@RequestLine("GET /sns/userinfo?access_token={access_token}&openid={openid}&lang=zh_CN")
	UserInfo getUserInfo(@Param(value = "access_token") String access_token, @Param(value = "openid") String openid);

}
