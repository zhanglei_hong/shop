package com.shop.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.shop.entity.wx.WxUser;

public class LoginUtil {

	public static Long getUserId() {
		return getSysUser().getId();
	}

	public static WxUser getSysUser() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		WxUser wxUser = (WxUser) request.getSession().getAttribute("wxUser");
		request.getSession().setAttribute("wxUser", wxUser);
		return wxUser;
	}
}
