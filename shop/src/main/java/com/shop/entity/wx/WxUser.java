 package com.shop.entity.wx;
import java.util.Date;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "wx_user")
public class WxUser {
	
 	//
 	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
 	//
	private String openid;
 	//
	private String nickname;
 	//
	private String sex;
 	//
	private String province;
 	//
	private String city;
 	//
	private String country;
 	//
	private String headimgurl;
 	//权限分号分割
	private String privilege;
 	//
	private String unionid;
 	//
	private Date createTime;
 	//
	private Date updateTime;
	
	public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
	public String getOpenid() {
        return this.openid;
    }
    public void setOpenid(String openid) {
        this.openid = openid;
    }
	public String getNickname() {
        return this.nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
	public String getSex() {
        return this.sex;
    }
    public void setSex(String sex) {
        this.sex = sex;
    }
	public String getProvince() {
        return this.province;
    }
    public void setProvince(String province) {
        this.province = province;
    }
	public String getCity() {
        return this.city;
    }
    public void setCity(String city) {
        this.city = city;
    }
	public String getCountry() {
        return this.country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
	public String getHeadimgurl() {
        return this.headimgurl;
    }
    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }
	public String getPrivilege() {
        return this.privilege;
    }
    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }
 
	public String getUnionid() {
		return unionid;
	}
	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}
	public Date getCreateTime() {
        return this.createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
	public Date getUpdateTime() {
        return this.updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}