 package com.shop.entity.shop;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "shop_details")
public class ShopDetails {
	
 	//
 	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
 	//图片
	@Column(name = "`img`")
	private String img;
 	//
	@Column(name = "`name`")
	private String name;
 	//金额
	@Column(name = "`money`")
	private String money;
 	//面积
	@Column(name = "`area`")
	private String area;
 	//单价
	@Column(name = "`price`")
	private String price;
 	//类别
	@Column(name = "`type`")
	private String type;
 	//押金
	@Column(name = "`deposit`")
	private String deposit;
 	//标签，多个;分割
	@Column(name = "`label`")
	private String label;
 	//详情
	@Column(name = "`details`")
	private String details;
 	//状态  1正常，2过期
	@Column(name = "`status`")
	private Integer status;
 	//
	@Column(name = "`create_time`")
	private Date createTime;
 	//
	@Column(name = "`update_time`")
	private Date updateTime;
	
	public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
	public String getImg() {
        return this.img;
    }
    public void setImg(String img) {
        this.img = img;
    }
	public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
	public String getMoney() {
        return this.money;
    }
    public void setMoney(String money) {
        this.money = money;
    }
	public String getArea() {
        return this.area;
    }
    public void setArea(String area) {
        this.area = area;
    }
	public String getPrice() {
        return this.price;
    }
    public void setPrice(String price) {
        this.price = price;
    }
	public String getType() {
        return this.type;
    }
    public void setType(String type) {
        this.type = type;
    }
	public String getDeposit() {
        return this.deposit;
    }
    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }
	public String getLabel() {
        return this.label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
	public String getDetails() {
        return this.details;
    }
    public void setDetails(String details) {
        this.details = details;
    }
	public Integer getStatus() {
        return this.status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
	public Date getCreateTime() {
        return this.createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
	public Date getUpdateTime() {
        return this.updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}