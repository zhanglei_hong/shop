 package com.shop.entity.shop;
import java.util.Date;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "shop_goods_class")
public class ShopGoodsClass {
	
 	//
 	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
 	//
	private String name;
 	//
	private Integer seq;
 	//
	private Date createTime;
 	//
	private Date updateTime;
	
	public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
	public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
	public Integer getSeq() {
        return this.seq;
    }
    public void setSeq(Integer seq) {
        this.seq = seq;
    }
	public Date getCreateTime() {
        return this.createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
	public Date getUpdateTime() {
        return this.updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}