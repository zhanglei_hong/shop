 package com.shop.entity.shop;
import java.util.Date;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "shop_company")
public class ShopCompany {
	
 	//
 	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
 	//
	private String name;
 	//性质
	private String nature;
 	//规模
	private String scale;
 	//行业
	private String industry;
 	//
	private String address;
 	//
	private String details;
 	//
	private Date createTime;
 	//
	private Date updateTime;
	
	public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
	public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
	public String getNature() {
        return this.nature;
    }
    public void setNature(String nature) {
        this.nature = nature;
    }
	public String getScale() {
        return this.scale;
    }
    public void setScale(String scale) {
        this.scale = scale;
    }
	public String getIndustry() {
        return this.industry;
    }
    public void setIndustry(String industry) {
        this.industry = industry;
    }
	public String getAddress() {
        return this.address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
	public String getDetails() {
        return this.details;
    }
    public void setDetails(String details) {
        this.details = details;
    }
	public Date getCreateTime() {
        return this.createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
	public Date getUpdateTime() {
        return this.updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}