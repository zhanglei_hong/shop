package com.shop.entity.sys;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "sys_district")
public class SysDistrict {

	//
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	// 父及地区关系
	@Column(name = "`pid`")
	private Long pid;
	// 地区名称
	@Column(name = "`name`")
	private String name;
	// 子属级别关系
	@Column(name = "`level`")
	private Integer level;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPid() {
		return this.pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

 

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getLevel() {
		return this.level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

}