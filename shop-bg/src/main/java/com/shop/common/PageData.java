package com.shop.common;

import java.util.Collection;
import java.util.List;

import com.github.pagehelper.Page;

public class PageData<T> {
	private Long total = 0l;
	private List<T> rows;

	public PageData(List<T> list) {
		if (list instanceof Page) {
			Page<T> page = (Page<T>) list;
			this.total = page.getTotal();
			rows = page.getResult();
		} else if (list instanceof Collection) {
			rows = list;
		}
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public List<T> getRows() {
		return rows;
	}

	public void setRows(List<T> rows) {
		this.rows = rows;
	}

}
