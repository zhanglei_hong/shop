package com.shop.common;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:env.properties")
public class Env {

	private static String filePath;

	@Value("${filePath}")
	private String filePathOrg;

 

	@PostConstruct
	public void initEnv() {
		filePath = this.filePathOrg;
	}



	public static String getFilePath() {
		return filePath;
	}

	public static void setFilePath(String filePath) {
		Env.filePath = filePath;
	}

 
}
