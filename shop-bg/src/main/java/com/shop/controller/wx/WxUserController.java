package com.shop.controller.wx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shop.common.PageData;
import com.shop.entity.wx.WxUser;
import com.shop.service.wx.WxUserService;

@RestController
@RequestMapping("/wxUser")
public class WxUserController {

	@Autowired
	private WxUserService wxUserService;

	@PostMapping(value = "/getWxUserList")
	public PageData<WxUser> getWxUserList(WxUser wxUser) {
		return wxUserService.getWxUserList(wxUser);
	}
}
