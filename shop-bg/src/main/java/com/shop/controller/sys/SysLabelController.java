package com.shop.controller.sys;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shop.entity.sys.SysLabel;
import com.shop.service.sys.SysLabelService;

@RestController
@RequestMapping("/sysLabel")
public class SysLabelController {

	@Autowired
	private SysLabelService sysLabelService;
	
	@RequestMapping(value = "/getAllSysLabel")
	public List<SysLabel> getAllSysLabel() {
		return sysLabelService.selectAll();
	}
}
