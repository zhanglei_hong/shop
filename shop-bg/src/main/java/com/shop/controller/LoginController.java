package com.shop.controller;

import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shop.entity.sys.SysUser;
import com.shop.util.SessionUtil;

@RestController
public class LoginController {

	/**
	 * 根据id查询用户信息
	 * 
	 * @param id
	 * @return
	 */
	@PostMapping("/login")
	public Integer login(String account, String password, HttpSession session) {

		return 0;
	}

	@GetMapping(value = "/getUser")
	public SysUser getUser() {
		return SessionUtil.getSysUser();
	}

	/**
	 * 退出
	 * 
	 * @return
	 */
	@GetMapping(value = "/logout")
	public Integer logout(HttpSession session) {
		session.removeAttribute("sysUser");
		return 1;
	}

}