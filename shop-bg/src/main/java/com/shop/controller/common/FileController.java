package com.shop.controller.common;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.shop.common.Env;
import com.shop.common.UUIDUtil;

@Controller
@RequestMapping("/file")
public class FileController {

	@PostMapping(value = "/upload")
	@ResponseBody
	public String upload(@RequestParam("file") MultipartFile multipartFile) {
		try {

			String uuid = UUIDUtil.getUuid();
			String originalFilename = multipartFile.getOriginalFilename();
			String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
			String fileName = uuid + suffix;
			File file = new File(Env.getFilePath(), fileName);
			// 写入文件
			multipartFile.transferTo(file);
			return fileName;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "0";
	}

	@GetMapping("/show/{fileName}")
	public void getFile(@PathVariable String fileName, HttpServletResponse response) {
		File file = new File(Env.getFilePath(), fileName);
		if (!file.exists()) {
			return;
		}
		response.setContentType("application/force-download");// 设置强制下载不打开            
		response.addHeader("Content-Disposition", "attachment;fileName=" + fileName);
		byte[] buffer = new byte[1024];
		FileInputStream fis = null;
		BufferedInputStream bis = null;
		try {
			fis = new FileInputStream(file);
			bis = new BufferedInputStream(fis);
			OutputStream outputStream = response.getOutputStream();
			int i = bis.read(buffer);
			while (i != -1) {
				outputStream.write(buffer, 0, i);
				i = bis.read(buffer);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (bis != null) {
			try {
				bis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (fis != null) {
			try {
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
