package com.shop.controller.shop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shop.common.PageData;
import com.shop.entity.shop.ShopDetails;
import com.shop.service.shop.ShopDetailsService;

@RestController
@RequestMapping("/shopDetails")
public class ShopDetailsController {

	@Autowired
	private ShopDetailsService shopDetailsService;
	
	
	@PostMapping(value = "/getShopDetailsList")
	public PageData<ShopDetails> getShopDetailsList(ShopDetails shopDetails) {
		return shopDetailsService.getShopDetailsList(shopDetails);
	}
	
	
	
	@PostMapping(value = "/saveShopDetails")
	public Integer saveShopDetails(ShopDetails shopDetails) {
		return shopDetailsService.saveShopDetails(shopDetails);
	}

	@GetMapping(value = "/getShopDetailsById")
	public ShopDetails getShopDetailsById(Long id) {
		return shopDetailsService.selectByPrimaryKey(id);
	}
	@GetMapping(value = "/deleteShopDetailsById")
	public Integer deleteShopDetailsById(Long id) {
		return shopDetailsService.deleteShopDetailsById(id);
	}
	
}
