package com.shop.controller.shop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shop.common.PageData;
import com.shop.entity.shop.ShopRecruitment;
import com.shop.service.shop.ShopRecruitmentService;

@RestController
@RequestMapping("/shopRecruitment")
public class ShopRecruitmentController {

	@Autowired
	private ShopRecruitmentService shopRecruitmentService;

	
	@PostMapping(value = "/getShopRecruitmentList")
	public PageData<ShopRecruitment> getShopRecruitmentList(ShopRecruitment shopRecruitment) {
		return shopRecruitmentService.getShopRecruitmentList(shopRecruitment);
	}
	
	@PostMapping(value = "/saveShopRecruitment")
	public Integer saveShopRecruitment(ShopRecruitment shopRecruitment) {
		return shopRecruitmentService.saveShopRecruitment(shopRecruitment);
	}

	@GetMapping(value = "/getShopRecruitmentById")
	public ShopRecruitment getShopRecruitmentById(Long id) {
		return shopRecruitmentService.selectByPrimaryKey(id);
	}
	@GetMapping(value = "/deleteShopRecruitmentById")
	public Integer deleteShopRecruitmentById(Long id) {
		return shopRecruitmentService.deleteShopRecruitmentById(id);
	}
}
