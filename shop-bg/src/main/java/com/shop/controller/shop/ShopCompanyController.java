package com.shop.controller.shop;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shop.common.PageData;
import com.shop.entity.shop.ShopCompany;
import com.shop.service.shop.ShopCompanyService;

@RestController
@RequestMapping("/shopCompany")
public class ShopCompanyController {

	@Autowired
	private ShopCompanyService shopCompanyService;

	@PostMapping(value = "/getShopCompanyList")
	public PageData<ShopCompany> getShopCompanyList(ShopCompany shopCompany) {
		return shopCompanyService.getShopCompanyList(shopCompany);
	}

	
	@PostMapping(value = "/getAllShopCompanyList")
	public List<ShopCompany> getAllShopCompanyList() {
		return shopCompanyService.getAllShopCompanyList();
	}
	
	@PostMapping(value = "/saveShopCompany")
	public Integer saveShopCompany(ShopCompany shopCompany) {
		return shopCompanyService.saveShopCompany(shopCompany);
	}

	@GetMapping(value = "/getShopCompanyById")
	public ShopCompany getShopCompanyById(Long id) {
		return shopCompanyService.selectByPrimaryKey(id);
	}
	@GetMapping(value = "/deleteShopCompanyById")
	public Integer deleteShopCompanyById(Long id) {
		return shopCompanyService.deleteShopCompanyById(id);
	}
}
