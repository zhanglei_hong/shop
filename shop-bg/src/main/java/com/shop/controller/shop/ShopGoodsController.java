package com.shop.controller.shop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shop.common.PageData;
import com.shop.entity.shop.ShopGoods;
import com.shop.service.shop.ShopGoodsService;

@RestController
@RequestMapping("/shopGoods")
public class ShopGoodsController {

	@Autowired
	private ShopGoodsService shopGoodsService;
	
	@PostMapping(value = "/getShopGoodsList")
	public PageData<ShopGoods> getShopGoodsList(ShopGoods shopGoods) {
		return shopGoodsService.getShopGoodsList(shopGoods);
	}
	
	@PostMapping(value = "/saveShopGoods")
	public Integer saveShopGoods(ShopGoods shopGoods) {
		return shopGoodsService.saveShopGoods(shopGoods);
	}

	@GetMapping(value = "/getShopGoodsById")
	public ShopGoods getShopGoodsById(Long id) {
		return shopGoodsService.selectByPrimaryKey(id);
	}
	@GetMapping(value = "/deleteShopGoodsById")
	public Integer deleteShopGoodsById(Long id) {
		return shopGoodsService.deleteShopGoodsById(id);
	}
}
