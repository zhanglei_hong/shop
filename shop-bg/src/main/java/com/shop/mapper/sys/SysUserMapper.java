package com.shop.mapper.sys;

import com.shop.common.BaseMapper;
import com.shop.entity.sys.SysUser;

public interface SysUserMapper extends BaseMapper<SysUser> {

}