package com.shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan("com.shop.mapper")
public class RunWeb {
	public static void main(String[] args) {
		SpringApplication.run(RunWeb.class, args);
	}
}