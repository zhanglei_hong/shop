package com.shop.service.wx;


import com.github.pagehelper.PageInfo;
import com.shop.common.PageData;
import com.shop.entity.wx.WxUser;
import com.shop.service.base.BaseService;

public interface WxUserService extends BaseService<WxUser> {

	PageData<WxUser> getWxUserList(WxUser wxUser);

}