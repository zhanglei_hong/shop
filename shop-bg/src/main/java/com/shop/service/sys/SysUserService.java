package com.shop.service.sys;


import com.shop.entity.sys.SysUser;
import com.shop.service.base.BaseService;

public interface SysUserService extends BaseService<SysUser> {

}