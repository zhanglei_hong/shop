package com.shop.service.sys;


import com.shop.entity.sys.SysLabel;
import com.shop.service.base.BaseService;

public interface SysLabelService extends BaseService<SysLabel> {

}