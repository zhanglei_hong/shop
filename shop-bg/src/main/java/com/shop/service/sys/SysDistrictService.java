package com.shop.service.sys;


import java.util.List;

import com.shop.entity.sys.SysDistrict;
import com.shop.service.base.BaseService;

public interface SysDistrictService extends BaseService<SysDistrict> {

	List<SysDistrict> getProvince();

	List<SysDistrict> getCity(Long pid);

}