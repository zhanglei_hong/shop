package com.shop.service.shop;


import com.shop.common.PageData;
import com.shop.entity.shop.ShopRecruitment;
import com.shop.service.base.BaseService;

public interface ShopRecruitmentService extends BaseService<ShopRecruitment> {

	PageData<ShopRecruitment> getShopRecruitmentList(ShopRecruitment shopRecruitment);

	Integer saveShopRecruitment(ShopRecruitment shopRecruitment);

	Integer deleteShopRecruitmentById(Long id);


}