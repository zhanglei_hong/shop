package com.shop.service.shop;


import com.shop.common.PageData;
import com.shop.entity.shop.ShopGoods;
import com.shop.service.base.BaseService;

public interface ShopGoodsService extends BaseService<ShopGoods> {

	PageData<ShopGoods> getShopGoodsList(ShopGoods shopGoods);

	Integer saveShopGoods(ShopGoods shopGoods);

	Integer deleteShopGoodsById(Long id);

}