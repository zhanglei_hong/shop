package com.shop.service.shop;


import java.util.List;

import com.shop.common.PageData;
import com.shop.entity.shop.ShopCompany;
import com.shop.service.base.BaseService;

public interface ShopCompanyService extends BaseService<ShopCompany> {

	PageData<ShopCompany> getShopCompanyList(ShopCompany shopCompany);

	Integer saveShopCompany(ShopCompany shopCompany);

	Integer deleteShopCompanyById(Long id);

	List<ShopCompany> getAllShopCompanyList();

}