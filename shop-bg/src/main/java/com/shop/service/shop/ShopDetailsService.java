package com.shop.service.shop;


import com.shop.common.PageData;
import com.shop.entity.shop.ShopDetails;
import com.shop.service.base.BaseService;

public interface ShopDetailsService extends BaseService<ShopDetails> {

	PageData<ShopDetails> getShopDetailsList(ShopDetails shopDetails);

	Integer saveShopDetails(ShopDetails shopDetails);

	Integer deleteShopDetailsById(Long id);

}