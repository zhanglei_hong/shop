package com.shop.service.base;

import com.shop.common.BaseMapper;

public interface BaseService<T> extends BaseMapper<T>  {

}
