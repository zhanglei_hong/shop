package com.shop.service.impl.shop;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.shop.common.Page;
import com.shop.common.PageData;
import com.shop.entity.shop.ShopDetails;
import com.shop.mapper.shop.ShopDetailsMapper;
import com.shop.service.base.BaseServiceImpl;
import com.shop.service.shop.ShopDetailsService;
import com.shop.util.SessionUtil;

import tk.mybatis.mapper.entity.Example;

@Service
public class ShopDetailsServiceImpl extends BaseServiceImpl<ShopDetails> implements ShopDetailsService {
	@Autowired
	private ShopDetailsMapper shopDetailsMapper;

	@Override
	public PageData<ShopDetails> getShopDetailsList(ShopDetails data) {
		Page page = SessionUtil.getPage();
		PageHelper.startPage(page.getPage(), page.getRows(), true);
		Example example = new Example(ShopDetails.class);
		Example.Criteria criteria = example.createCriteria();
		String name = data.getName();
		if (name != null && !"".equals(name)) {
			criteria.andLike("name", "%" + name + "%");
		}
		criteria.andEqualTo("status", 1);
		List<ShopDetails> list = shopDetailsMapper.selectByExample(example);
		PageData<ShopDetails> pageData = new PageData<ShopDetails>(list);
		return pageData;

	}

	@Override
	public Integer saveShopDetails(ShopDetails shopDetails) {
		Long id = shopDetails.getId();
		shopDetails.setUpdateTime(new Date());
		if (id != null) {
			shopDetailsMapper.updateByPrimaryKeySelective(shopDetails);
		} else {
			shopDetails.setCreateTime(new Date());
			shopDetailsMapper.insertSelective(shopDetails);
		}
		return 1;
	}

	@Override
	public Integer deleteShopDetailsById(Long id) {
		ShopDetails data = new ShopDetails();
		data.setId(id);
		data.setStatus(2);
		data.setUpdateTime(new Date());
		return shopDetailsMapper.updateByPrimaryKeySelective(data);
	}

}