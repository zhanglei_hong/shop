package com.shop.service.impl.shop;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.shop.common.Page;
import com.shop.common.PageData;
import com.shop.entity.shop.ShopRecruitment;
import com.shop.mapper.shop.ShopRecruitmentMapper;
import com.shop.service.base.BaseServiceImpl;
import com.shop.service.shop.ShopRecruitmentService;
import com.shop.util.SessionUtil;

import tk.mybatis.mapper.entity.Example;

@Service
public class ShopRecruitmentServiceImpl extends BaseServiceImpl<ShopRecruitment> implements ShopRecruitmentService {
	@Autowired
	private ShopRecruitmentMapper shopRecruitmentMapper;

	@Override
	public PageData<ShopRecruitment> getShopRecruitmentList(ShopRecruitment data) {

		Page page = SessionUtil.getPage();
		PageHelper.startPage(page.getPage(), page.getRows(), true);
		Example example = new Example(ShopRecruitment.class);
		Example.Criteria criteria = example.createCriteria();
		String name = data.getName();
		if (name != null && !"".equals(name)) {
			criteria.andLike("name", "%" + name + "%");
		}
		criteria.andEqualTo("status", 1);
		List<ShopRecruitment> list = shopRecruitmentMapper.selectByExample(example);
		PageData<ShopRecruitment> pageData = new PageData<ShopRecruitment>(list);
		return pageData;
	}

	@Override
	public Integer saveShopRecruitment(ShopRecruitment shopRecruitment) {
		Long id = shopRecruitment.getId();
		shopRecruitment.setUpdateTime(new Date());
		if (id != null) {
			shopRecruitmentMapper.updateByPrimaryKeySelective(shopRecruitment);
		}else {
			shopRecruitment.setCreateTime(new Date());
			shopRecruitmentMapper.insertSelective(shopRecruitment);
		}
		return 1;
	}

	@Override
	public Integer deleteShopRecruitmentById(Long id) {
		ShopRecruitment data = new ShopRecruitment();
		data.setId(id);
		data.setStatus(2);
		data.setUpdateTime(new Date());
		return shopRecruitmentMapper.updateByPrimaryKeySelective(data);
	}

}