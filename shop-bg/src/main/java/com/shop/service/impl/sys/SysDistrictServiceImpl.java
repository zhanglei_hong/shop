package com.shop.service.impl.sys;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shop.entity.sys.SysDistrict;
import com.shop.mapper.sys.SysDistrictMapper;
import com.shop.service.base.BaseServiceImpl;
import com.shop.service.sys.SysDistrictService;

import tk.mybatis.mapper.entity.Example;

@Service
public class SysDistrictServiceImpl extends BaseServiceImpl<SysDistrict> implements SysDistrictService {
	@Autowired
	private SysDistrictMapper sysDistrictMapper;

	@Override
	public List<SysDistrict> getProvince() {
		return getCity(1l);
	}

	@Override
	public List<SysDistrict> getCity(Long pid) {
		Example example = new Example(SysDistrict.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andEqualTo("pid", pid);
		List<SysDistrict> list = sysDistrictMapper.selectByExample(example);
		return list;
	}

}