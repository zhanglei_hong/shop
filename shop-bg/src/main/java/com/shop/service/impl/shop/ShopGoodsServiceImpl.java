package com.shop.service.impl.shop;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.shop.common.Page;
import com.shop.common.PageData;
import com.shop.entity.shop.ShopGoods;
import com.shop.mapper.shop.ShopGoodsMapper;
import com.shop.service.base.BaseServiceImpl;
import com.shop.service.shop.ShopGoodsService;
import com.shop.util.SessionUtil;

import tk.mybatis.mapper.entity.Example;

@Service
public class ShopGoodsServiceImpl extends BaseServiceImpl<ShopGoods> implements ShopGoodsService {
	@Autowired
	private ShopGoodsMapper shopGoodsMapper;


	@Override
	public PageData<ShopGoods> getShopGoodsList(ShopGoods data) {
		Page page=SessionUtil.getPage();
		PageHelper.startPage(page.getPage(), page.getRows(), true);
		Example example = new Example(ShopGoods.class);
		Example.Criteria criteria = example.createCriteria();
		String name = data.getName();
		if (name != null && !"".equals(name)) {
			criteria.andLike("name", "%" + name + "%");
		}
		criteria.andEqualTo("status", 1);
		List<ShopGoods> list = shopGoodsMapper.selectByExample(example);
		PageData<ShopGoods> pageData =new  PageData<ShopGoods>(list);
		return pageData;
		 
	}
	@Override
	public Integer saveShopGoods(ShopGoods shopGoods) {
		Long id = shopGoods.getId();
		shopGoods.setUpdateTime(new Date());
		if (id != null) {
			shopGoodsMapper.updateByPrimaryKeySelective(shopGoods);
		}else {
			shopGoods.setCreateTime(new Date());
			shopGoodsMapper.insertSelective(shopGoods);
		}
		return 1;
	}

	@Override
	public Integer deleteShopGoodsById(Long id) {
		ShopGoods data=new ShopGoods();
		data.setId(id);
		data.setStatus(2);
		data.setUpdateTime(new Date());
		return shopGoodsMapper.updateByPrimaryKeySelective(data);
	}
}