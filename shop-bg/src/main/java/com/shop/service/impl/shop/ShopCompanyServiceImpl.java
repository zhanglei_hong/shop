package com.shop.service.impl.shop;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.shop.common.Page;
import com.shop.common.PageData;
import com.shop.entity.shop.ShopCompany;
import com.shop.mapper.shop.ShopCompanyMapper;
import com.shop.service.base.BaseServiceImpl;
import com.shop.service.shop.ShopCompanyService;
import com.shop.util.SessionUtil;

import tk.mybatis.mapper.entity.Example;

@Service
public class ShopCompanyServiceImpl extends BaseServiceImpl<ShopCompany> implements ShopCompanyService {
	@Autowired
	private ShopCompanyMapper shopCompanyMapper;

	@Override
	public PageData<ShopCompany> getShopCompanyList(ShopCompany data) {
		Page page = SessionUtil.getPage();
		PageHelper.startPage(page.getPage(), page.getRows(), true);
		Example example = new Example(ShopCompany.class);
		Example.Criteria criteria = example.createCriteria();
		String name = data.getName();
		if (name != null && !"".equals(name)) {
			criteria.andLike("name", "%" + name + "%");
		}
		criteria.andEqualTo("status", 1);
		List<ShopCompany> list = shopCompanyMapper.selectByExample(example);
		PageData<ShopCompany> pageData = new PageData<ShopCompany>(list);
		return pageData;
	}

	@Override
	public Integer saveShopCompany(ShopCompany shopCompany) {
		Long id = shopCompany.getId();
		shopCompany.setUpdateTime(new Date());
		if (id != null) {
			shopCompanyMapper.updateByPrimaryKeySelective(shopCompany);
		}else {
			shopCompany.setCreateTime(new Date());
			shopCompanyMapper.insertSelective(shopCompany);
		}
		return 1;
	}

	@Override
	public Integer deleteShopCompanyById(Long id) {
		ShopCompany data=new ShopCompany();
		data.setId(id);
		data.setStatus(2);
		data.setUpdateTime(new Date());
		return shopCompanyMapper.updateByPrimaryKeySelective(data);
	}

	@Override
	public List<ShopCompany> getAllShopCompanyList() {
		Example example = new Example(ShopCompany.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andEqualTo("status", 1);
		List<ShopCompany> list = shopCompanyMapper.selectByExample(example);
		return list;
	}

}