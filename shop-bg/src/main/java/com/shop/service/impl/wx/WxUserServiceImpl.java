package com.shop.service.impl.wx;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.shop.common.Page;
import com.shop.common.PageData;
import com.shop.entity.wx.WxUser;
import com.shop.mapper.wx.WxUserMapper;
import com.shop.service.base.BaseServiceImpl;
import com.shop.service.wx.WxUserService;
import com.shop.util.SessionUtil;

import tk.mybatis.mapper.entity.Example;

@Service
public class WxUserServiceImpl extends BaseServiceImpl<WxUser> implements WxUserService {
	@Autowired
	private WxUserMapper wxUserMapper;

	@Override
	public PageData<WxUser> getWxUserList(WxUser data) {
		Page page=SessionUtil.getPage();
		PageHelper.startPage(page.getPage(), page.getRows(), true);
		Example example = new Example(WxUser.class);
		Example.Criteria criteria = example.createCriteria();
		String nickname = data.getNickname();
		if (nickname != null && !"".equals(nickname)) {
			criteria.andLike("nickname", "%" + nickname + "%");
		}

		String province = data.getProvince();
		if (province != null && !"".equals(province)) {
			criteria.andLike("province", "%" + province + "%");
		}

		String city = data.getCity();
		if (city != null && !"".equals(city)) {
			criteria.andLike("city", "%" + city + "%");
		}
		String sex = data.getSex();
		if (sex != null && !"".equals(sex)) {
			criteria.andEqualTo("sex", sex);
		}
		List<WxUser> list = wxUserMapper.selectByExample(example);
		PageData<WxUser> pageData =new  PageData<WxUser>(list);
		return pageData;
	}

}