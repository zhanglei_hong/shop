 package com.shop.entity.shop;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "shop_recruitment")
public class ShopRecruitment {
	
 	//
 	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
 	//
	@Column(name = "`name`")
	private String name;
 	//薪资
	@Column(name = "`salary`")
	private String salary;
 	//经验
	@Column(name = "`experience`")
	private Integer experience;
 	//学历
	@Column(name = "`education`")
	private Integer education;
 	//省
	@Column(name = "`province`")
	private Long province;
 	//市区
	@Column(name = "`city`")
	private Long city;
 	//公司id
	@Column(name = "`company_id`")
	private Long companyId;
 	//1置顶
	@Column(name = "`roof`")
	private Integer roof;
 	//类别 1急招
	@Column(name = "`type`")
	private Integer type;
 	//状态  1正常，2过期
	@Column(name = "`status`")
	private Integer status;
 	//
	@Column(name = "`details`")
	private String details;

 	//1 一周以内，2一个月以内
	@Column(name = "`entry_time`")
	private Integer entryTime;
  
 	//
	@Column(name = "`create_time`")
	private Date createTime;
 	//
	@Column(name = "`update_time`")
	private Date updateTime;
	
	public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
	public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
	public String getSalary() {
        return this.salary;
    }
    public void setSalary(String salary) {
        this.salary = salary;
    }
	public Integer getExperience() {
        return this.experience;
    }
    public void setExperience(Integer experience) {
        this.experience = experience;
    }
	public Integer getEducation() {
        return this.education;
    }
    public void setEducation(Integer education) {
        this.education = education;
    }
	public Long getProvince() {
        return this.province;
    }
    public void setProvince(Long province) {
        this.province = province;
    }
	public Long getCity() {
        return this.city;
    }
    public void setCity(Long city) {
        this.city = city;
    }
	public Long getCompanyId() {
        return this.companyId;
    }
    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
	public Integer getRoof() {
        return this.roof;
    }
    public void setRoof(Integer roof) {
        this.roof = roof;
    }
	public Integer getType() {
        return this.type;
    }
    public void setType(Integer type) {
        this.type = type;
    }
	public Integer getStatus() {
        return this.status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
	public String getDetails() {
        return this.details;
    }
    public void setDetails(String details) {
        this.details = details;
    }
	 
	public Integer getEntryTime() {
        return this.entryTime;
    }
    public void setEntryTime(Integer entryTime) {
        this.entryTime = entryTime;
    }
 
	public Date getCreateTime() {
        return this.createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
	public Date getUpdateTime() {
        return this.updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}