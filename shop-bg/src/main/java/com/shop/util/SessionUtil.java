package com.shop.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.shop.common.Page;
import com.shop.entity.sys.SysUser;

/**
 * session
 * 
 * @author zhanglei
 *
 */
public class SessionUtil {

	public static Long getUserId() {
		return getSysUser().getId();
	}

	public static Page getPage() {
		Page page = new Page();
		HttpServletRequest request = getRequest();
		String pageStr = request.getParameter("page");
		if (pageStr != null && !"".equals(pageStr)) {
			page.setPage(Integer.valueOf(pageStr));
		}
		String rowsStr = request.getParameter("rows");
		if (rowsStr != null && !"".equals(rowsStr)) {
			page.setRows(Integer.valueOf(rowsStr));
		}
		return page;
	}

	public static SysUser getSysUser() {
		SysUser wxUser = (SysUser) getRequest().getSession().getAttribute("sysUser");
		return wxUser;
	}

	public static HttpServletRequest getRequest() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		return request;
	}

}
