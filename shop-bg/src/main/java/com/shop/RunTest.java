package com.shop;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.shop.entity.sys.SysUser;
import com.shop.service.sys.SysUserService;
import com.shop.util.Md5;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RunTest {
	@Autowired
	private SysUserService sysUserService;

	@Test
	public void testRun() {
		SysUser sysUser=new  SysUser();
		sysUser.setName("测试");
		sysUser.setPassword(Md5.getMD5("123"));
		sysUser.setAccount("acc");
		sysUser.setCreateTime(new Date());
		sysUser.setUpdateTime(new Date());
		sysUserService.insert(sysUser);
	}

//	@Test
//	public void testFeign() {
//		WechatFeign wechatFeign = Feign.builder().encoder(new JacksonEncoder()).decoder(new JacksonDecoder())
//				.options(new Request.Options(1000, 3500)).retryer(new Retryer.Default(5000, 5000, 3))
//				.target(WechatFeign.class, "http://10.0.10.108:9200");
//
//	}
}
