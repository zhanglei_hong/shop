function formatterTime(value,row,index) {
		return getMyDate(value);
}

function formatterImgurl(value,row,index) {
	return "<img src='/file/show/"+value+"' style='height: 50px;width: 50px;'  />";
}
function formatterHeadimgurl(value,row,index) {
	return "<img src='"+value+"' style='height: 50px;width: 50px;'  />";
}
function formatterSex(value,row,index) {
	if (value=="1") {
		return "男";
	}else if(value=="2"){
		return "女";
	}else{
		return "未知";
	}
}


function formatterEducation(value,row,index) {
	if (value=="1") {
		return "大专";
	}else if(value=="2"){
		return "本科";
	}else if(value=="3"){
		return "硕士";
	}else if(value=="4"){
		return "博士";
	}else{
		return "不要求";
	}
}

function formatterExperience(value,row,index) {
	if (value=="1") {
		return "应届毕业生";
	}else if(value=="2"){
		return "3年及以下";
	}else if(value=="3"){
		return "3-5年";
	}else if(value=="4"){
		return "5-10年";
	}else if(value=="5"){
		return "10年以上";
	}else{
		return "不要求";
	}
}

function formatterIsYes(value,row,index) {
	if (value=="1") {
		return "是";
	}else{
		return "否";
	}
}
 
function formatterEntryTime(value,row,index) {
	if (value=="1") {
		return "一周以内";
	}else if(value=="2"){
		return "一个月以内";
	}else{
		return "";
	}
}
function getMyDate(str) {
	    var oDate = new Date(str),
	    oYear = oDate.getFullYear(),
	    oMonth = oDate.getMonth()+1,
	    oDay = oDate.getDate(),
	    oHour = oDate.getHours(),
	    oMin = oDate.getMinutes(),
	    oSen = oDate.getSeconds(),
	    oTime = oYear +'-'+ addZero(oMonth) +'-'+ addZero(oDay) +' '+ addZero(oHour) +':'+
		addZero(oMin) +':'+addZero(oSen);
	    return oTime;
	}

	// 补零操作
	function addZero(num){
	    if(parseInt(num) < 10){
	        num = '0'+num;
	    }
	    return num;
	}
 