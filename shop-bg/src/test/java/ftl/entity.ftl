 package com.shop.entity.${packagePath};
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "${tableName}")
public class ${className} {
	
 	<#list columnList as col>
 	//${col.columnComment}
 	<#if col.changeColumnName =="id">
 	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	<#else>
	@Column(name = "`${col.columnName}`")
 	</#if>
	private ${col.type} ${col.changeColumnName};
	</#list> 
	
 	<#list columnList as col>
	public ${col.type} get${col.CapitalColumnName}() {
        return this.${col.changeColumnName};
    }
    public void set${col.CapitalColumnName}(${col.type} ${col.changeColumnName}) {
        this.${col.changeColumnName} = ${col.changeColumnName};
    }
	</#list> 

}