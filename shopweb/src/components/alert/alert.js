import Vue from 'vue'
import alertComponent from './alert.vue'

let instance;
const loadingConstructor = Vue.extend(alertComponent)
instance = new loadingConstructor({
    el : document.createElement('div')
})
instance.show = false

const alert = {
    show(options = {}) {
        instance.show = true
        instance.options = options
        if(options) {
            document.body.appendChild(instance.$el)
        }
    },
    hide() {
        let thisNode=document.getElementById("alert_div")
        thisNode && thisNode.parentNode.removeChild(thisNode)
        instance.show = false
    }
}

export default {
    install() {
        if(!Vue.$alert) {
            Vue.$alert = alert
        }
        Vue.mixin({
            created() {
                this.$alert = Vue.$alert
            }
        })
    }
}