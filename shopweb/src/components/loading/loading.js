import Vue from 'vue'
import loadingComponent from './loading.vue'

let instance;
const loadingConstructor = Vue.extend(loadingComponent)
instance = new loadingConstructor({
    el : document.createElement('div')
})
instance.show = false

const loading = {
    show(options = {}) {
        instance.show = true
        if(options) {
            document.body.appendChild(instance.$el)
        }
    },
    hide() {
        instance.show = false
        let thisNode=document.getElementById("loading_div")
        thisNode && thisNode.parentNode.removeChild(thisNode)
    }
}

export default {
    install() {
        if(!Vue.$loading) {
            Vue.$loading = loading
        }
        Vue.mixin({
            created() {
                this.$loading = Vue.$loading
            }
        })
    }
}