import axios from 'axios'
import qs from 'qs'
import Vue from 'vue'

const timeout = 10000
const fetch = axios.create({
    timeout,
    headers: {
        'Content-Type': 'application/json'
    },
    transformRequest: [function (data) {
        console.log("data",data)
        // return data.stringify(data)
        return JSON.stringify(data)
    }]
})
// 请求拦截
fetch.interceptors.request.use(function(config) {
    Vue.$loading.show()
    return config;
}, function(error) {
    return Promise.reject(error)
})

// 响应截断器
fetch.interceptors.response.use(function(response) {
    Vue.$loading.hide()
    if(response.data.errcode == 0) {
        return Promise.resolve(response.data)
    }else{
        // Vue.$alert.show({
        //     content : response.data.errmsg || '网络开小差'
        // })
        return Promise.resolve(response.data)
    }
}, function(error) {
    Vue.$loading.hide()
    return Promise.reject(error)
})

export default fetch