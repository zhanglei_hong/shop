import fetch from '../service/service'
import urls from '../api/urls'
//我的
export function getUser(params = {}) {
    return fetch.get(urls.getUser,{params})
}
//首页
export function getShopList(params = {}) {
    return fetch.post(urls.getShopList,params)
}
export function getShopGoodsById(params = {}) {
    return fetch.get(urls.getShopGoodsById,{params})
}
export function getShopDetailsList(params = {}) {
    return fetch.post(urls.getShopDetailsList,params)
}
export function getShopRecruitmentList(params = {}) {
    return fetch.post(urls.getShopRecruitmentList,params)
}
export function getShopDetailsById(params = {}) {
    return fetch.get(urls.getShopDetailsById,{params})
}
export function getShopRecruitmentById(params = {}) {
    return fetch.get(urls.getShopRecruitmentById,{params})
}
export function getProvince(params = {}) {
    return fetch.get(urls.getProvince,{params})
}
export function getCity(params = {}) {
    return fetch.get(urls.getCity,{params})
}
export function getAllCity(params = {}) {
    return fetch.get(urls.getAllCity,{params})
}