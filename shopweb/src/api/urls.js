import config from '../config/config'

let url = config.url

export default {
    //我的
    getUser : `${url}/wxUser/getUser`,
    //首页
    getShopList : `${url}/shopGoods/getShopGoodsList`,
    getShopGoodsById : `${url}/shopGoods/getShopGoodsById`,
    //转让
    getShopDetailsList : `${url}/shopDetails/getShopDetailsList`,
    getShopDetailsById : `${url}/shopDetails/getShopDetailsById`,
    
    //招聘
    getShopRecruitmentList : `${url}/shopRecruitment/getShopRecruitmentList`,
    getShopRecruitmentById : `${url}/shopRecruitment/getShopRecruitmentById`,
    getProvince : `${url}/sysDistrict/getProvince`,
    getCity : `${url}/sysDistrict/getCity`,
    getAllCity : `${url}/sysDistrict/getAllCity`
   
   
    

}