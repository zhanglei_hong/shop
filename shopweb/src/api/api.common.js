import fetch from '../service/service'
import config from '../config/config'
import axios from 'axios'
export function getWxConfig() {
   
    let url = `${config.api}`
    let params = {
        appid : config.appid,
        token : config.token
    }
    return axios.get(url, {params})
}