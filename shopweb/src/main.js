import Vue from 'vue'
import App from './App.vue'
import router from './router'
import loading from './components/loading/loading.js'
import alert from './components/alert/alert.js'
import './assets/css/reset.css'
import 'lib-flexible/flexible.js'

import fastClick from 'fastclick'
fastClick.attach(document.body)

import NutUI from '@nutui/nutui';
import '@nutui/nutui/dist/nutui.css';

NutUI.install(Vue);

Vue.config.productionTip = false
Vue.use(loading)
Vue.use(alert)
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
