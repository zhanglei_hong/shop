import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect : '/index'
    },
    {
      path: '/index',
      name: 'index',
      component: () => import('./views/Index.vue')
    },
    //找工作
    {
      path: '/lookjob',
      name: 'lookjob',
      component: () => import('./views/LookJob.vue')
    },
    {
      path: '/jobdetails',
      name: 'jobdetails',
      component: () => import('./views/JobDetails.vue')
    },
    {
      path: '/main',
      name: 'main',
      component: () => import('./views/Main.vue')
    },
    {
      path: '/indexdetails',
      name: 'indexdetails',
      component: () => import('./views/IndexDetails.vue')
    },
    {
      path: '/second',
      name: 'second',
      component: () => import('./views/Second.vue')
    },
    {
      path: '/secondDetails',
      name: 'secondDetails',
      component: () => import('./views/SecondDetails.vue')
    }
  ]
})
