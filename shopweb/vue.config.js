const path = require('path')
module.exports = {
    publicPath: "./",
    configureWebpack: {
        // output: { // 输出重构  打包编译后的 文件名称  【模块名称】
        //     filename: `js/[name].js`,
        //     chunkFilename: `js/[name].js` 
        // },
    },
    filenameHashing : false,
    productionSourceMap: false
};