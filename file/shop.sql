/*
Navicat MySQL Data Transfer

Source Server         : 本地数据库
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : shop

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2019-10-04 13:39:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for shop_company
-- ----------------------------
DROP TABLE IF EXISTS `shop_company`;
CREATE TABLE `shop_company` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `nature` varchar(100) NOT NULL COMMENT '性质',
  `scale` varchar(100) DEFAULT NULL COMMENT '规模',
  `industry` varchar(100) DEFAULT NULL COMMENT '行业',
  `address` varchar(100) DEFAULT NULL,
  `details` varchar(500) DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1 正常 2删除',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_company
-- ----------------------------
INSERT INTO `shop_company` VALUES ('1', '大公司', '132111', '13211', '312', '312', '323', '2', '2019-10-02 06:54:36', '2019-10-02 09:48:48');
INSERT INTO `shop_company` VALUES ('2', 'ags', '1', '1', '1', '1', '112', '1', '2019-10-03 11:59:37', '2019-10-03 11:59:37');

-- ----------------------------
-- Table structure for shop_details
-- ----------------------------
DROP TABLE IF EXISTS `shop_details`;
CREATE TABLE `shop_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `img` varchar(500) DEFAULT NULL COMMENT '图片',
  `name` varchar(100) DEFAULT NULL,
  `money` varchar(100) DEFAULT NULL COMMENT '金额',
  `area` varchar(100) DEFAULT NULL COMMENT '面积',
  `price` varchar(100) DEFAULT NULL COMMENT '单价',
  `type` varchar(100) DEFAULT NULL COMMENT '类别',
  `deposit` varchar(100) DEFAULT NULL COMMENT '押金',
  `label` varchar(500) DEFAULT NULL COMMENT '标签，多个;分割',
  `details` varchar(500) DEFAULT NULL COMMENT '详情',
  `status` int(11) DEFAULT '1' COMMENT '状态  1正常，2过期',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='招聘';

-- ----------------------------
-- Records of shop_details
-- ----------------------------

-- ----------------------------
-- Table structure for shop_goods
-- ----------------------------
DROP TABLE IF EXISTS `shop_goods`;
CREATE TABLE `shop_goods` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `img` varchar(100) NOT NULL,
  `price` double(6,2) DEFAULT NULL,
  `goods_class_id` bigint(20) DEFAULT NULL COMMENT '分类id',
  `details` varchar(500) DEFAULT NULL COMMENT '详情',
  `status` int(11) DEFAULT '1' COMMENT '1 正常 2删除',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_goods
-- ----------------------------

-- ----------------------------
-- Table structure for shop_goods_class
-- ----------------------------
DROP TABLE IF EXISTS `shop_goods_class`;
CREATE TABLE `shop_goods_class` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `seq` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_goods_class
-- ----------------------------

-- ----------------------------
-- Table structure for shop_recruitment
-- ----------------------------
DROP TABLE IF EXISTS `shop_recruitment`;
CREATE TABLE `shop_recruitment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `salary` varchar(100) DEFAULT NULL COMMENT '薪资',
  `experience` int(11) DEFAULT NULL COMMENT '经验',
  `education` int(11) DEFAULT NULL COMMENT '学历',
  `province` bigint(20) DEFAULT NULL COMMENT '省',
  `city` bigint(20) DEFAULT NULL COMMENT '市区',
  `company_id` bigint(20) DEFAULT NULL COMMENT '公司id',
  `roof` int(11) DEFAULT '0' COMMENT '1置顶',
  `type` int(1) DEFAULT '0' COMMENT '类别 1急招',
  `status` int(11) DEFAULT '1' COMMENT '状态  1正常，2过期',
  `details` varchar(500) DEFAULT NULL,
  `entry_time` int(11) DEFAULT NULL COMMENT '1 一周以内，2一个月以内',
  `marital_status` int(11) DEFAULT '0' COMMENT '0 不限 1 已婚 2 未婚',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='招聘';

-- ----------------------------
-- Records of shop_recruitment
-- ----------------------------
INSERT INTO `shop_recruitment` VALUES ('4', '1', '1', '2', '2', '3', '36', '2', '0', '0', '2', '', '2', '0', '2019-10-04 01:12:52', '2019-10-04 02:27:48');
INSERT INTO `shop_recruitment` VALUES ('5', '1', '21', '1', '2', '6', '80', '2', '0', '0', '1', '111', '1', '0', '2019-10-04 01:13:06', '2019-10-04 03:01:51');
INSERT INTO `shop_recruitment` VALUES ('6', '111', '123', '2', '2', '11', '161', '2', '0', '0', '1', '21312', '2', '0', '2019-10-04 03:03:22', '2019-10-04 03:03:22');

-- ----------------------------
-- Table structure for sys_district
-- ----------------------------
DROP TABLE IF EXISTS `sys_district`;
CREATE TABLE `sys_district` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pid` bigint(5) unsigned NOT NULL DEFAULT '0' COMMENT '父及地区关系',
  `name` varchar(120) NOT NULL DEFAULT '' COMMENT '地区名称',
  `level` int(1) NOT NULL COMMENT '子属级别关系',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=398 DEFAULT CHARSET=utf8 COMMENT='全国地区表';

-- ----------------------------
-- Records of sys_district
-- ----------------------------
INSERT INTO `sys_district` VALUES ('1', '0', '中国', '0');
INSERT INTO `sys_district` VALUES ('2', '1', '北京市', '1');
INSERT INTO `sys_district` VALUES ('3', '1', '安徽省', '1');
INSERT INTO `sys_district` VALUES ('4', '1', '福建省', '1');
INSERT INTO `sys_district` VALUES ('5', '1', '甘肃省', '1');
INSERT INTO `sys_district` VALUES ('6', '1', '广东省', '1');
INSERT INTO `sys_district` VALUES ('7', '1', '广西壮族自治区', '1');
INSERT INTO `sys_district` VALUES ('8', '1', '贵州省', '1');
INSERT INTO `sys_district` VALUES ('9', '1', '海南省', '1');
INSERT INTO `sys_district` VALUES ('10', '1', '河北省', '1');
INSERT INTO `sys_district` VALUES ('11', '1', '河南省', '1');
INSERT INTO `sys_district` VALUES ('12', '1', '黑龙江省', '1');
INSERT INTO `sys_district` VALUES ('13', '1', '湖北省', '1');
INSERT INTO `sys_district` VALUES ('14', '1', '湖南省', '1');
INSERT INTO `sys_district` VALUES ('15', '1', '吉林省', '1');
INSERT INTO `sys_district` VALUES ('16', '1', '江苏省', '1');
INSERT INTO `sys_district` VALUES ('17', '1', '江西省', '1');
INSERT INTO `sys_district` VALUES ('18', '1', '辽宁省', '1');
INSERT INTO `sys_district` VALUES ('19', '1', '内蒙古自治区', '1');
INSERT INTO `sys_district` VALUES ('20', '1', '宁夏回族自治区', '1');
INSERT INTO `sys_district` VALUES ('21', '1', '青海省', '1');
INSERT INTO `sys_district` VALUES ('22', '1', '山东省', '1');
INSERT INTO `sys_district` VALUES ('23', '1', '山西省', '1');
INSERT INTO `sys_district` VALUES ('24', '1', '陕西省', '1');
INSERT INTO `sys_district` VALUES ('25', '1', '上海市', '1');
INSERT INTO `sys_district` VALUES ('26', '1', '四川省', '1');
INSERT INTO `sys_district` VALUES ('27', '1', '天津市', '1');
INSERT INTO `sys_district` VALUES ('28', '1', '西藏自治区', '1');
INSERT INTO `sys_district` VALUES ('29', '1', '新疆维吾尔自治区', '1');
INSERT INTO `sys_district` VALUES ('30', '1', '云南省', '1');
INSERT INTO `sys_district` VALUES ('31', '1', '浙江省', '1');
INSERT INTO `sys_district` VALUES ('32', '1', '重庆市', '1');
INSERT INTO `sys_district` VALUES ('33', '1', '香港特别行政区', '1');
INSERT INTO `sys_district` VALUES ('34', '1', '澳门特别行政区', '1');
INSERT INTO `sys_district` VALUES ('35', '1', '台湾省', '1');
INSERT INTO `sys_district` VALUES ('36', '3', '安庆', '2');
INSERT INTO `sys_district` VALUES ('37', '3', '蚌埠', '2');
INSERT INTO `sys_district` VALUES ('38', '3', '巢湖', '2');
INSERT INTO `sys_district` VALUES ('39', '3', '池州', '2');
INSERT INTO `sys_district` VALUES ('40', '3', '滁州', '2');
INSERT INTO `sys_district` VALUES ('41', '3', '阜阳', '2');
INSERT INTO `sys_district` VALUES ('42', '3', '淮北', '2');
INSERT INTO `sys_district` VALUES ('43', '3', '淮南', '2');
INSERT INTO `sys_district` VALUES ('44', '3', '黄山', '2');
INSERT INTO `sys_district` VALUES ('45', '3', '六安', '2');
INSERT INTO `sys_district` VALUES ('46', '3', '马鞍山', '2');
INSERT INTO `sys_district` VALUES ('47', '3', '宿州', '2');
INSERT INTO `sys_district` VALUES ('48', '3', '铜陵', '2');
INSERT INTO `sys_district` VALUES ('49', '3', '芜湖', '2');
INSERT INTO `sys_district` VALUES ('50', '3', '宣城', '2');
INSERT INTO `sys_district` VALUES ('51', '3', '亳州', '2');
INSERT INTO `sys_district` VALUES ('52', '2', '北京', '2');
INSERT INTO `sys_district` VALUES ('53', '4', '福州', '2');
INSERT INTO `sys_district` VALUES ('54', '4', '龙岩', '2');
INSERT INTO `sys_district` VALUES ('55', '4', '南平', '2');
INSERT INTO `sys_district` VALUES ('56', '4', '宁德', '2');
INSERT INTO `sys_district` VALUES ('57', '4', '莆田', '2');
INSERT INTO `sys_district` VALUES ('58', '4', '泉州', '2');
INSERT INTO `sys_district` VALUES ('59', '4', '三明', '2');
INSERT INTO `sys_district` VALUES ('60', '4', '厦门', '2');
INSERT INTO `sys_district` VALUES ('61', '4', '漳州', '2');
INSERT INTO `sys_district` VALUES ('62', '5', '兰州', '2');
INSERT INTO `sys_district` VALUES ('63', '5', '白银', '2');
INSERT INTO `sys_district` VALUES ('64', '5', '定西', '2');
INSERT INTO `sys_district` VALUES ('65', '5', '甘南', '2');
INSERT INTO `sys_district` VALUES ('66', '5', '嘉峪关', '2');
INSERT INTO `sys_district` VALUES ('67', '5', '金昌', '2');
INSERT INTO `sys_district` VALUES ('68', '5', '酒泉', '2');
INSERT INTO `sys_district` VALUES ('69', '5', '临夏', '2');
INSERT INTO `sys_district` VALUES ('70', '5', '陇南', '2');
INSERT INTO `sys_district` VALUES ('71', '5', '平凉', '2');
INSERT INTO `sys_district` VALUES ('72', '5', '庆阳', '2');
INSERT INTO `sys_district` VALUES ('73', '5', '天水', '2');
INSERT INTO `sys_district` VALUES ('74', '5', '武威', '2');
INSERT INTO `sys_district` VALUES ('75', '5', '张掖', '2');
INSERT INTO `sys_district` VALUES ('76', '6', '广州', '2');
INSERT INTO `sys_district` VALUES ('77', '6', '深圳', '2');
INSERT INTO `sys_district` VALUES ('78', '6', '潮州', '2');
INSERT INTO `sys_district` VALUES ('79', '6', '东莞', '2');
INSERT INTO `sys_district` VALUES ('80', '6', '佛山', '2');
INSERT INTO `sys_district` VALUES ('81', '6', '河源', '2');
INSERT INTO `sys_district` VALUES ('82', '6', '惠州', '2');
INSERT INTO `sys_district` VALUES ('83', '6', '江门', '2');
INSERT INTO `sys_district` VALUES ('84', '6', '揭阳', '2');
INSERT INTO `sys_district` VALUES ('85', '6', '茂名', '2');
INSERT INTO `sys_district` VALUES ('86', '6', '梅州', '2');
INSERT INTO `sys_district` VALUES ('87', '6', '清远', '2');
INSERT INTO `sys_district` VALUES ('88', '6', '汕头', '2');
INSERT INTO `sys_district` VALUES ('89', '6', '汕尾', '2');
INSERT INTO `sys_district` VALUES ('90', '6', '韶关', '2');
INSERT INTO `sys_district` VALUES ('91', '6', '阳江', '2');
INSERT INTO `sys_district` VALUES ('92', '6', '云浮', '2');
INSERT INTO `sys_district` VALUES ('93', '6', '湛江', '2');
INSERT INTO `sys_district` VALUES ('94', '6', '肇庆', '2');
INSERT INTO `sys_district` VALUES ('95', '6', '中山', '2');
INSERT INTO `sys_district` VALUES ('96', '6', '珠海', '2');
INSERT INTO `sys_district` VALUES ('97', '7', '南宁', '2');
INSERT INTO `sys_district` VALUES ('98', '7', '桂林', '2');
INSERT INTO `sys_district` VALUES ('99', '7', '百色', '2');
INSERT INTO `sys_district` VALUES ('100', '7', '北海', '2');
INSERT INTO `sys_district` VALUES ('101', '7', '崇左', '2');
INSERT INTO `sys_district` VALUES ('102', '7', '防城港', '2');
INSERT INTO `sys_district` VALUES ('103', '7', '贵港', '2');
INSERT INTO `sys_district` VALUES ('104', '7', '河池', '2');
INSERT INTO `sys_district` VALUES ('105', '7', '贺州', '2');
INSERT INTO `sys_district` VALUES ('106', '7', '来宾', '2');
INSERT INTO `sys_district` VALUES ('107', '7', '柳州', '2');
INSERT INTO `sys_district` VALUES ('108', '7', '钦州', '2');
INSERT INTO `sys_district` VALUES ('109', '7', '梧州', '2');
INSERT INTO `sys_district` VALUES ('110', '7', '玉林', '2');
INSERT INTO `sys_district` VALUES ('111', '8', '贵阳', '2');
INSERT INTO `sys_district` VALUES ('112', '8', '安顺', '2');
INSERT INTO `sys_district` VALUES ('113', '8', '毕节', '2');
INSERT INTO `sys_district` VALUES ('114', '8', '六盘水', '2');
INSERT INTO `sys_district` VALUES ('115', '8', '黔东南', '2');
INSERT INTO `sys_district` VALUES ('116', '8', '黔南', '2');
INSERT INTO `sys_district` VALUES ('117', '8', '黔西南', '2');
INSERT INTO `sys_district` VALUES ('118', '8', '铜仁', '2');
INSERT INTO `sys_district` VALUES ('119', '8', '遵义', '2');
INSERT INTO `sys_district` VALUES ('120', '9', '海口', '2');
INSERT INTO `sys_district` VALUES ('121', '9', '三亚', '2');
INSERT INTO `sys_district` VALUES ('122', '9', '白沙', '2');
INSERT INTO `sys_district` VALUES ('123', '9', '保亭', '2');
INSERT INTO `sys_district` VALUES ('124', '9', '昌江', '2');
INSERT INTO `sys_district` VALUES ('125', '9', '澄迈县', '2');
INSERT INTO `sys_district` VALUES ('126', '9', '定安县', '2');
INSERT INTO `sys_district` VALUES ('127', '9', '东方', '2');
INSERT INTO `sys_district` VALUES ('128', '9', '乐东', '2');
INSERT INTO `sys_district` VALUES ('129', '9', '临高县', '2');
INSERT INTO `sys_district` VALUES ('130', '9', '陵水', '2');
INSERT INTO `sys_district` VALUES ('131', '9', '琼海', '2');
INSERT INTO `sys_district` VALUES ('132', '9', '琼中', '2');
INSERT INTO `sys_district` VALUES ('133', '9', '屯昌县', '2');
INSERT INTO `sys_district` VALUES ('134', '9', '万宁', '2');
INSERT INTO `sys_district` VALUES ('135', '9', '文昌', '2');
INSERT INTO `sys_district` VALUES ('136', '9', '五指山', '2');
INSERT INTO `sys_district` VALUES ('137', '9', '儋州', '2');
INSERT INTO `sys_district` VALUES ('138', '10', '石家庄', '2');
INSERT INTO `sys_district` VALUES ('139', '10', '保定', '2');
INSERT INTO `sys_district` VALUES ('140', '10', '沧州', '2');
INSERT INTO `sys_district` VALUES ('141', '10', '承德', '2');
INSERT INTO `sys_district` VALUES ('142', '10', '邯郸', '2');
INSERT INTO `sys_district` VALUES ('143', '10', '衡水', '2');
INSERT INTO `sys_district` VALUES ('144', '10', '廊坊', '2');
INSERT INTO `sys_district` VALUES ('145', '10', '秦皇岛', '2');
INSERT INTO `sys_district` VALUES ('146', '10', '唐山', '2');
INSERT INTO `sys_district` VALUES ('147', '10', '邢台', '2');
INSERT INTO `sys_district` VALUES ('148', '10', '张家口', '2');
INSERT INTO `sys_district` VALUES ('149', '11', '郑州', '2');
INSERT INTO `sys_district` VALUES ('150', '11', '洛阳', '2');
INSERT INTO `sys_district` VALUES ('151', '11', '开封', '2');
INSERT INTO `sys_district` VALUES ('152', '11', '安阳', '2');
INSERT INTO `sys_district` VALUES ('153', '11', '鹤壁', '2');
INSERT INTO `sys_district` VALUES ('154', '11', '济源', '2');
INSERT INTO `sys_district` VALUES ('155', '11', '焦作', '2');
INSERT INTO `sys_district` VALUES ('156', '11', '南阳', '2');
INSERT INTO `sys_district` VALUES ('157', '11', '平顶山', '2');
INSERT INTO `sys_district` VALUES ('158', '11', '三门峡', '2');
INSERT INTO `sys_district` VALUES ('159', '11', '商丘', '2');
INSERT INTO `sys_district` VALUES ('160', '11', '新乡', '2');
INSERT INTO `sys_district` VALUES ('161', '11', '信阳', '2');
INSERT INTO `sys_district` VALUES ('162', '11', '许昌', '2');
INSERT INTO `sys_district` VALUES ('163', '11', '周口', '2');
INSERT INTO `sys_district` VALUES ('164', '11', '驻马店', '2');
INSERT INTO `sys_district` VALUES ('165', '11', '漯河', '2');
INSERT INTO `sys_district` VALUES ('166', '11', '濮阳', '2');
INSERT INTO `sys_district` VALUES ('167', '12', '哈尔滨', '2');
INSERT INTO `sys_district` VALUES ('168', '12', '大庆', '2');
INSERT INTO `sys_district` VALUES ('169', '12', '大兴安岭', '2');
INSERT INTO `sys_district` VALUES ('170', '12', '鹤岗', '2');
INSERT INTO `sys_district` VALUES ('171', '12', '黑河', '2');
INSERT INTO `sys_district` VALUES ('172', '12', '鸡西', '2');
INSERT INTO `sys_district` VALUES ('173', '12', '佳木斯', '2');
INSERT INTO `sys_district` VALUES ('174', '12', '牡丹江', '2');
INSERT INTO `sys_district` VALUES ('175', '12', '七台河', '2');
INSERT INTO `sys_district` VALUES ('176', '12', '齐齐哈尔', '2');
INSERT INTO `sys_district` VALUES ('177', '12', '双鸭山', '2');
INSERT INTO `sys_district` VALUES ('178', '12', '绥化', '2');
INSERT INTO `sys_district` VALUES ('179', '12', '伊春', '2');
INSERT INTO `sys_district` VALUES ('180', '13', '武汉', '2');
INSERT INTO `sys_district` VALUES ('181', '13', '仙桃', '2');
INSERT INTO `sys_district` VALUES ('182', '13', '鄂州', '2');
INSERT INTO `sys_district` VALUES ('183', '13', '黄冈', '2');
INSERT INTO `sys_district` VALUES ('184', '13', '黄石', '2');
INSERT INTO `sys_district` VALUES ('185', '13', '荆门', '2');
INSERT INTO `sys_district` VALUES ('186', '13', '荆州', '2');
INSERT INTO `sys_district` VALUES ('187', '13', '潜江', '2');
INSERT INTO `sys_district` VALUES ('188', '13', '神农架林区', '2');
INSERT INTO `sys_district` VALUES ('189', '13', '十堰', '2');
INSERT INTO `sys_district` VALUES ('190', '13', '随州', '2');
INSERT INTO `sys_district` VALUES ('191', '13', '天门', '2');
INSERT INTO `sys_district` VALUES ('192', '13', '咸宁', '2');
INSERT INTO `sys_district` VALUES ('193', '13', '襄樊', '2');
INSERT INTO `sys_district` VALUES ('194', '13', '孝感', '2');
INSERT INTO `sys_district` VALUES ('195', '13', '宜昌', '2');
INSERT INTO `sys_district` VALUES ('196', '13', '恩施', '2');
INSERT INTO `sys_district` VALUES ('197', '14', '长沙', '2');
INSERT INTO `sys_district` VALUES ('198', '14', '张家界', '2');
INSERT INTO `sys_district` VALUES ('199', '14', '常德', '2');
INSERT INTO `sys_district` VALUES ('200', '14', '郴州', '2');
INSERT INTO `sys_district` VALUES ('201', '14', '衡阳', '2');
INSERT INTO `sys_district` VALUES ('202', '14', '怀化', '2');
INSERT INTO `sys_district` VALUES ('203', '14', '娄底', '2');
INSERT INTO `sys_district` VALUES ('204', '14', '邵阳', '2');
INSERT INTO `sys_district` VALUES ('205', '14', '湘潭', '2');
INSERT INTO `sys_district` VALUES ('206', '14', '湘西', '2');
INSERT INTO `sys_district` VALUES ('207', '14', '益阳', '2');
INSERT INTO `sys_district` VALUES ('208', '14', '永州', '2');
INSERT INTO `sys_district` VALUES ('209', '14', '岳阳', '2');
INSERT INTO `sys_district` VALUES ('210', '14', '株洲', '2');
INSERT INTO `sys_district` VALUES ('211', '15', '长春', '2');
INSERT INTO `sys_district` VALUES ('212', '15', '吉林', '2');
INSERT INTO `sys_district` VALUES ('213', '15', '白城', '2');
INSERT INTO `sys_district` VALUES ('214', '15', '白山', '2');
INSERT INTO `sys_district` VALUES ('215', '15', '辽源', '2');
INSERT INTO `sys_district` VALUES ('216', '15', '四平', '2');
INSERT INTO `sys_district` VALUES ('217', '15', '松原', '2');
INSERT INTO `sys_district` VALUES ('218', '15', '通化', '2');
INSERT INTO `sys_district` VALUES ('219', '15', '延边', '2');
INSERT INTO `sys_district` VALUES ('220', '16', '南京', '2');
INSERT INTO `sys_district` VALUES ('221', '16', '苏州', '2');
INSERT INTO `sys_district` VALUES ('222', '16', '无锡', '2');
INSERT INTO `sys_district` VALUES ('223', '16', '常州', '2');
INSERT INTO `sys_district` VALUES ('224', '16', '淮安', '2');
INSERT INTO `sys_district` VALUES ('225', '16', '连云港', '2');
INSERT INTO `sys_district` VALUES ('226', '16', '南通', '2');
INSERT INTO `sys_district` VALUES ('227', '16', '宿迁', '2');
INSERT INTO `sys_district` VALUES ('228', '16', '泰州', '2');
INSERT INTO `sys_district` VALUES ('229', '16', '徐州', '2');
INSERT INTO `sys_district` VALUES ('230', '16', '盐城', '2');
INSERT INTO `sys_district` VALUES ('231', '16', '扬州', '2');
INSERT INTO `sys_district` VALUES ('232', '16', '镇江', '2');
INSERT INTO `sys_district` VALUES ('233', '17', '南昌', '2');
INSERT INTO `sys_district` VALUES ('234', '17', '抚州', '2');
INSERT INTO `sys_district` VALUES ('235', '17', '赣州', '2');
INSERT INTO `sys_district` VALUES ('236', '17', '吉安', '2');
INSERT INTO `sys_district` VALUES ('237', '17', '景德镇', '2');
INSERT INTO `sys_district` VALUES ('238', '17', '九江', '2');
INSERT INTO `sys_district` VALUES ('239', '17', '萍乡', '2');
INSERT INTO `sys_district` VALUES ('240', '17', '上饶', '2');
INSERT INTO `sys_district` VALUES ('241', '17', '新余', '2');
INSERT INTO `sys_district` VALUES ('242', '17', '宜春', '2');
INSERT INTO `sys_district` VALUES ('243', '17', '鹰潭', '2');
INSERT INTO `sys_district` VALUES ('244', '18', '沈阳', '2');
INSERT INTO `sys_district` VALUES ('245', '18', '大连', '2');
INSERT INTO `sys_district` VALUES ('246', '18', '鞍山', '2');
INSERT INTO `sys_district` VALUES ('247', '18', '本溪', '2');
INSERT INTO `sys_district` VALUES ('248', '18', '朝阳', '2');
INSERT INTO `sys_district` VALUES ('249', '18', '丹东', '2');
INSERT INTO `sys_district` VALUES ('250', '18', '抚顺', '2');
INSERT INTO `sys_district` VALUES ('251', '18', '阜新', '2');
INSERT INTO `sys_district` VALUES ('252', '18', '葫芦岛', '2');
INSERT INTO `sys_district` VALUES ('253', '18', '锦州', '2');
INSERT INTO `sys_district` VALUES ('254', '18', '辽阳', '2');
INSERT INTO `sys_district` VALUES ('255', '18', '盘锦', '2');
INSERT INTO `sys_district` VALUES ('256', '18', '铁岭', '2');
INSERT INTO `sys_district` VALUES ('257', '18', '营口', '2');
INSERT INTO `sys_district` VALUES ('258', '19', '呼和浩特', '2');
INSERT INTO `sys_district` VALUES ('259', '19', '阿拉善盟', '2');
INSERT INTO `sys_district` VALUES ('260', '19', '巴彦淖尔盟', '2');
INSERT INTO `sys_district` VALUES ('261', '19', '包头', '2');
INSERT INTO `sys_district` VALUES ('262', '19', '赤峰', '2');
INSERT INTO `sys_district` VALUES ('263', '19', '鄂尔多斯', '2');
INSERT INTO `sys_district` VALUES ('264', '19', '呼伦贝尔', '2');
INSERT INTO `sys_district` VALUES ('265', '19', '通辽', '2');
INSERT INTO `sys_district` VALUES ('266', '19', '乌海', '2');
INSERT INTO `sys_district` VALUES ('267', '19', '乌兰察布市', '2');
INSERT INTO `sys_district` VALUES ('268', '19', '锡林郭勒盟', '2');
INSERT INTO `sys_district` VALUES ('269', '19', '兴安盟', '2');
INSERT INTO `sys_district` VALUES ('270', '20', '银川', '2');
INSERT INTO `sys_district` VALUES ('271', '20', '固原', '2');
INSERT INTO `sys_district` VALUES ('272', '20', '石嘴山', '2');
INSERT INTO `sys_district` VALUES ('273', '20', '吴忠', '2');
INSERT INTO `sys_district` VALUES ('274', '20', '中卫', '2');
INSERT INTO `sys_district` VALUES ('275', '21', '西宁', '2');
INSERT INTO `sys_district` VALUES ('276', '21', '果洛', '2');
INSERT INTO `sys_district` VALUES ('277', '21', '海北', '2');
INSERT INTO `sys_district` VALUES ('278', '21', '海东', '2');
INSERT INTO `sys_district` VALUES ('279', '21', '海南', '2');
INSERT INTO `sys_district` VALUES ('280', '21', '海西', '2');
INSERT INTO `sys_district` VALUES ('281', '21', '黄南', '2');
INSERT INTO `sys_district` VALUES ('282', '21', '玉树', '2');
INSERT INTO `sys_district` VALUES ('283', '22', '济南', '2');
INSERT INTO `sys_district` VALUES ('284', '22', '青岛', '2');
INSERT INTO `sys_district` VALUES ('285', '22', '滨州', '2');
INSERT INTO `sys_district` VALUES ('286', '22', '德州', '2');
INSERT INTO `sys_district` VALUES ('287', '22', '东营', '2');
INSERT INTO `sys_district` VALUES ('288', '22', '菏泽', '2');
INSERT INTO `sys_district` VALUES ('289', '22', '济宁', '2');
INSERT INTO `sys_district` VALUES ('290', '22', '莱芜', '2');
INSERT INTO `sys_district` VALUES ('291', '22', '聊城', '2');
INSERT INTO `sys_district` VALUES ('292', '22', '临沂', '2');
INSERT INTO `sys_district` VALUES ('293', '22', '日照', '2');
INSERT INTO `sys_district` VALUES ('294', '22', '泰安', '2');
INSERT INTO `sys_district` VALUES ('295', '22', '威海', '2');
INSERT INTO `sys_district` VALUES ('296', '22', '潍坊', '2');
INSERT INTO `sys_district` VALUES ('297', '22', '烟台', '2');
INSERT INTO `sys_district` VALUES ('298', '22', '枣庄', '2');
INSERT INTO `sys_district` VALUES ('299', '22', '淄博', '2');
INSERT INTO `sys_district` VALUES ('300', '23', '太原', '2');
INSERT INTO `sys_district` VALUES ('301', '23', '长治', '2');
INSERT INTO `sys_district` VALUES ('302', '23', '大同', '2');
INSERT INTO `sys_district` VALUES ('303', '23', '晋城', '2');
INSERT INTO `sys_district` VALUES ('304', '23', '晋中', '2');
INSERT INTO `sys_district` VALUES ('305', '23', '临汾', '2');
INSERT INTO `sys_district` VALUES ('306', '23', '吕梁', '2');
INSERT INTO `sys_district` VALUES ('307', '23', '朔州', '2');
INSERT INTO `sys_district` VALUES ('308', '23', '忻州', '2');
INSERT INTO `sys_district` VALUES ('309', '23', '阳泉', '2');
INSERT INTO `sys_district` VALUES ('310', '23', '运城', '2');
INSERT INTO `sys_district` VALUES ('311', '24', '西安', '2');
INSERT INTO `sys_district` VALUES ('312', '24', '安康', '2');
INSERT INTO `sys_district` VALUES ('313', '24', '宝鸡', '2');
INSERT INTO `sys_district` VALUES ('314', '24', '汉中', '2');
INSERT INTO `sys_district` VALUES ('315', '24', '商洛', '2');
INSERT INTO `sys_district` VALUES ('316', '24', '铜川', '2');
INSERT INTO `sys_district` VALUES ('317', '24', '渭南', '2');
INSERT INTO `sys_district` VALUES ('318', '24', '咸阳', '2');
INSERT INTO `sys_district` VALUES ('319', '24', '延安', '2');
INSERT INTO `sys_district` VALUES ('320', '24', '榆林', '2');
INSERT INTO `sys_district` VALUES ('321', '25', '上海', '2');
INSERT INTO `sys_district` VALUES ('322', '26', '成都', '2');
INSERT INTO `sys_district` VALUES ('323', '26', '绵阳', '2');
INSERT INTO `sys_district` VALUES ('324', '26', '阿坝', '2');
INSERT INTO `sys_district` VALUES ('325', '26', '巴中', '2');
INSERT INTO `sys_district` VALUES ('326', '26', '达州', '2');
INSERT INTO `sys_district` VALUES ('327', '26', '德阳', '2');
INSERT INTO `sys_district` VALUES ('328', '26', '甘孜', '2');
INSERT INTO `sys_district` VALUES ('329', '26', '广安', '2');
INSERT INTO `sys_district` VALUES ('330', '26', '广元', '2');
INSERT INTO `sys_district` VALUES ('331', '26', '乐山', '2');
INSERT INTO `sys_district` VALUES ('332', '26', '凉山', '2');
INSERT INTO `sys_district` VALUES ('333', '26', '眉山', '2');
INSERT INTO `sys_district` VALUES ('334', '26', '南充', '2');
INSERT INTO `sys_district` VALUES ('335', '26', '内江', '2');
INSERT INTO `sys_district` VALUES ('336', '26', '攀枝花', '2');
INSERT INTO `sys_district` VALUES ('337', '26', '遂宁', '2');
INSERT INTO `sys_district` VALUES ('338', '26', '雅安', '2');
INSERT INTO `sys_district` VALUES ('339', '26', '宜宾', '2');
INSERT INTO `sys_district` VALUES ('340', '26', '资阳', '2');
INSERT INTO `sys_district` VALUES ('341', '26', '自贡', '2');
INSERT INTO `sys_district` VALUES ('342', '26', '泸州', '2');
INSERT INTO `sys_district` VALUES ('343', '27', '天津', '2');
INSERT INTO `sys_district` VALUES ('344', '28', '拉萨', '2');
INSERT INTO `sys_district` VALUES ('345', '28', '阿里', '2');
INSERT INTO `sys_district` VALUES ('346', '28', '昌都', '2');
INSERT INTO `sys_district` VALUES ('347', '28', '林芝', '2');
INSERT INTO `sys_district` VALUES ('348', '28', '那曲', '2');
INSERT INTO `sys_district` VALUES ('349', '28', '日喀则', '2');
INSERT INTO `sys_district` VALUES ('350', '28', '山南', '2');
INSERT INTO `sys_district` VALUES ('351', '29', '乌鲁木齐', '2');
INSERT INTO `sys_district` VALUES ('352', '29', '阿克苏', '2');
INSERT INTO `sys_district` VALUES ('353', '29', '阿拉尔', '2');
INSERT INTO `sys_district` VALUES ('354', '29', '巴音郭楞', '2');
INSERT INTO `sys_district` VALUES ('355', '29', '博尔塔拉', '2');
INSERT INTO `sys_district` VALUES ('356', '29', '昌吉', '2');
INSERT INTO `sys_district` VALUES ('357', '29', '哈密', '2');
INSERT INTO `sys_district` VALUES ('358', '29', '和田', '2');
INSERT INTO `sys_district` VALUES ('359', '29', '喀什', '2');
INSERT INTO `sys_district` VALUES ('360', '29', '克拉玛依', '2');
INSERT INTO `sys_district` VALUES ('361', '29', '克孜勒苏', '2');
INSERT INTO `sys_district` VALUES ('362', '29', '石河子', '2');
INSERT INTO `sys_district` VALUES ('363', '29', '图木舒克', '2');
INSERT INTO `sys_district` VALUES ('364', '29', '吐鲁番', '2');
INSERT INTO `sys_district` VALUES ('365', '29', '五家渠', '2');
INSERT INTO `sys_district` VALUES ('366', '29', '伊犁', '2');
INSERT INTO `sys_district` VALUES ('367', '30', '昆明', '2');
INSERT INTO `sys_district` VALUES ('368', '30', '怒江', '2');
INSERT INTO `sys_district` VALUES ('369', '30', '普洱', '2');
INSERT INTO `sys_district` VALUES ('370', '30', '丽江', '2');
INSERT INTO `sys_district` VALUES ('371', '30', '保山', '2');
INSERT INTO `sys_district` VALUES ('372', '30', '楚雄', '2');
INSERT INTO `sys_district` VALUES ('373', '30', '大理', '2');
INSERT INTO `sys_district` VALUES ('374', '30', '德宏', '2');
INSERT INTO `sys_district` VALUES ('375', '30', '迪庆', '2');
INSERT INTO `sys_district` VALUES ('376', '30', '红河', '2');
INSERT INTO `sys_district` VALUES ('377', '30', '临沧', '2');
INSERT INTO `sys_district` VALUES ('378', '30', '曲靖', '2');
INSERT INTO `sys_district` VALUES ('379', '30', '文山', '2');
INSERT INTO `sys_district` VALUES ('380', '30', '西双版纳', '2');
INSERT INTO `sys_district` VALUES ('381', '30', '玉溪', '2');
INSERT INTO `sys_district` VALUES ('382', '30', '昭通', '2');
INSERT INTO `sys_district` VALUES ('383', '31', '杭州', '2');
INSERT INTO `sys_district` VALUES ('384', '31', '湖州', '2');
INSERT INTO `sys_district` VALUES ('385', '31', '嘉兴', '2');
INSERT INTO `sys_district` VALUES ('386', '31', '金华', '2');
INSERT INTO `sys_district` VALUES ('387', '31', '丽水', '2');
INSERT INTO `sys_district` VALUES ('388', '31', '宁波', '2');
INSERT INTO `sys_district` VALUES ('389', '31', '绍兴', '2');
INSERT INTO `sys_district` VALUES ('390', '31', '台州', '2');
INSERT INTO `sys_district` VALUES ('391', '31', '温州', '2');
INSERT INTO `sys_district` VALUES ('392', '31', '舟山', '2');
INSERT INTO `sys_district` VALUES ('393', '31', '衢州', '2');
INSERT INTO `sys_district` VALUES ('394', '32', '重庆', '2');
INSERT INTO `sys_district` VALUES ('395', '33', '香港', '2');
INSERT INTO `sys_district` VALUES ('396', '34', '澳门', '2');
INSERT INTO `sys_district` VALUES ('397', '35', '台湾', '2');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `account` varchar(100) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('3', '管理员', 'admin', '202cb962ac59075b964b07152d234b70', '2019-09-17 01:32:58', '2019-09-17 01:32:58');

-- ----------------------------
-- Table structure for wx_user
-- ----------------------------
DROP TABLE IF EXISTS `wx_user`;
CREATE TABLE `wx_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `openid` varchar(100) DEFAULT NULL,
  `nickname` varchar(100) NOT NULL,
  `sex` varchar(1) DEFAULT '0',
  `province` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `headimgurl` varchar(500) DEFAULT NULL,
  `privilege` varchar(500) DEFAULT NULL COMMENT '权限分号分割',
  `unionid` varchar(100) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `openid` (`openid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='微信表';

-- ----------------------------
-- Records of wx_user
-- ----------------------------
INSERT INTO `wx_user` VALUES ('2', 'onj2RuPtAi2kuYgxNcP1Y4va3JbY', '奇奇颗颗', '1', '北京', '', '中国', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJRu2nbqUXYB77pAapRMyKzDswRrlUI8JLwhTIWnOCEulDibZmFRzpggOsmQHicV7doam4UEeSuVQcw/132', '', null, '2019-09-26 07:29:30', '2019-09-26 07:29:30');

-- ----------------------------
-- Table structure for wx_user_copy
-- ----------------------------
DROP TABLE IF EXISTS `wx_user_copy`;
CREATE TABLE `wx_user_copy` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `openid` varchar(100) DEFAULT NULL,
  `nickname` varchar(100) NOT NULL,
  `sex` varchar(1) DEFAULT '0',
  `province` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `headimgurl` varchar(500) DEFAULT NULL,
  `privilege` varchar(500) DEFAULT NULL COMMENT '权限分号分割',
  `unionid` varchar(100) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `openid` (`openid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='微信表';

-- ----------------------------
-- Records of wx_user_copy
-- ----------------------------
INSERT INTO `wx_user_copy` VALUES ('2', 'onj2RuPtAi2kuYgxNcP1Y4va3JbY', '奇奇颗颗', '1', '北京', '', '中国', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJRu2nbqUXYB77pAapRMyKzDswRrlUI8JLwhTIWnOCEulDibZmFRzpggOsmQHicV7doam4UEeSuVQcw/132', '', null, '2019-09-26 07:29:30', '2019-09-26 07:29:30');
